#include "config.h"

#define M1 0   //电机编号
#define M2 1
#define M3 2
#define M4 3

#define DEFAULT_M1_STEPS_PER_MM 53.33333  //每单位移动需要的脉冲数
#define DEFAULT_M2_STEPS_PER_MM 88.88888
#define DEFAULT_M3_STEPS_PER_MM 25.0
#define DEFAULT_M4_STEPS_PER_MM 25.0
#define DEFAULT_M1_MAX_RATE 500.0 // mm/s   //移动最大速度
#define DEFAULT_M2_MAX_RATE 500.0 // mm/s
#define DEFAULT_M3_MAX_RATE 500.0 // mm/s
#define DEFAULT_M4_MAX_RATE 500.0 // mm/s
#define MOTOR_ACCELERATION 200
#define DEFAULT_M1_ACCELERATION 10 // 10 mm/sec^2  //移动加速度
#define DEFAULT_M2_ACCELERATION 10 // 10 mm/sec^2
#define DEFAULT_M3_ACCELERATION 10 // 10 mm/sec^2
#define DEFAULT_M4_ACCELERATION 10 // 10 mm/sec^2
#define DEFAULT_M1_MAX_TRAVEL 200.0 // mm  //单轴行程
#define DEFAULT_M2_MAX_TRAVEL 200.0 // mm
#define DEFAULT_M3_MAX_TRAVEL 200.0 // mm
#define DEFAULT_M4_MAX_TRAVEL 200.0 // mm	
	

	
extern unsigned char Motors_Dir[4];    //方向
extern unsigned char Motors_Busy;   //运行
extern unsigned long Motors_Cnt[4];    //脉冲数据
extern unsigned long ratio_n[4];
extern unsigned long STEP_n;          //总脉冲计数变量
extern unsigned char Motors_Fully;  //速度
extern float Motor_time_ratio[4];

extern long int Motors_Current[4]; //每轴绝对位置

extern long int Planning_Step[512];  //电机运动脉冲规划缓冲区
extern unsigned int Planning_Time[512];  //电机运动时间规划缓冲区

extern void Motors_Init(void);           //电机状态初始化
extern void Planning_Step_Motors(float M1_Target,float M2_Target,float M3_Target,float M4_Target,float Feed_Rate); //规划电机梯形运动
extern void Run_Moto(void);                   //电机产生运动
extern void Motors_Stop(unsigned char Chs);         //电机失能
extern void Motors_Start(unsigned char Chs);        //电机使能
extern void Motors_Home(unsigned char Chs,unsigned char Dir);        //电机回零