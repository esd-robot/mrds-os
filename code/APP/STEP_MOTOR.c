#include "STEP_MOTOR.h"

#define Section 50    //加速阶段每秒运动分段数


uchar Motors_Dir[4];    //方向标志
uchar Motors_Busy;   //运行标志
uint32 Motors_Cnt[4];    //脉冲数据
uint32 T0_n;             //定时器计数变量
uint32 Time0_step;      //每次要生成的脉冲数
uint32 ratio_n[4];        //各电机脉冲生成计数变量
uint32 STEP_n=0;          //总脉冲计数变量
uchar Motors_Fully;  //电机运转忙标志

long int Motors_Current[4]; //每轴当前绝对位置
long int Motors_target[4];          //每轴目标绝对位置

float M1_Current;  //每轴当前位置
float M2_Current;
float M3_Current;
float M4_Current;

uint M_num=0;

float Motor_time_ratio[4];              //各轴脉冲时间比例

long int Planning_Step[512];  //电机运动脉冲规划缓冲区
uint Planning_Time[512];  //电机运动时间规划缓冲区
uint Start_Flag =0;        //环形缓冲区头
uint End_Flag =0;          //环形缓冲区尾


void Planning_Step_Motors(float M1_Target,float M2_Target,float M3_Target,float M4_Target,float Feed_Rate)
{
	uint32 n,m;      //循环计数变量
	float M1_err, M2_err,M3_err,M4_err;   //目标与实际偏差
	long int S,S1,S2,S3;               //定义加减速阶段的脉冲数
	uint32 step_event_count;                //本次运动的最大脉冲数
	uchar Max_axis ;                        //本次运动脉冲数最多的轴
	float MAX_STEPS_PER_MM ;                //本次运动脉冲数最多的轴的单位脉冲  
//	float Cartesian_mm;                  //各轴运动的和路程
//	float Motor_Seconds;                 //总共运动时间
//	float Feed_Motor[4];                    //单轴速度 
	
	float V0 = 0.0;
	float Vt = 0.0;
	float Vm,Vn;                   //无匀速阶段的最大速度
	float T1,T2,T3;              //定义各阶段使用时间
	uint K1,K2,K3;               //定义各阶段步骤数
	
	M1_err = M1_Target-M1_Current;   //偏差=期望-实际
	M2_err = M2_Target-M2_Current;
	M3_err = M3_Target-M3_Current;
	M4_err = M4_Target-M4_Current;
	
	if(M1_err<0) Motors_Dir[M1]=0;     //获取电机运动方向
	else 	Motors_Dir[M1]=1;
	if(M2_err<0) Motors_Dir[M2]=0;
	else 	Motors_Dir[M2]=1;
	if(M3_err<0) Motors_Dir[M3]=0;
	else 	Motors_Dir[M3]=1;
	if(M4_err<0) Motors_Dir[M4]=0;
	else 	Motors_Dir[M4]=1;
	
	Motors_target[M1] = abs ((int)( M1_Target * DEFAULT_M1_STEPS_PER_MM));  //计算各轴目标位置（绝对值）=目标坐标 * 各轴每单位脉冲数
	Motors_target[M2] = abs ((int)( M2_Target * DEFAULT_M2_STEPS_PER_MM));
	Motors_target[M3] = abs ((int)( M3_Target * DEFAULT_M3_STEPS_PER_MM));
	Motors_target[M4] = abs ((int)( M4_Target * DEFAULT_M4_STEPS_PER_MM));
	
	Motors_Cnt[M1] =  abs ((int)(Motors_Current[M1]-Motors_target[M1]));  //计算每个轴电机运动步数 = 差值 * 每单位脉冲数
	Motors_Cnt[M2] =  abs ((int)(Motors_Current[M2]-Motors_target[M2]));
	Motors_Cnt[M3] =  abs ((int)(Motors_Current[M3]-Motors_target[M3]));
	Motors_Cnt[M4] =  abs ((int)(Motors_Current[M4]-Motors_target[M4]));
	
	step_event_count=0;
	Max_axis =0;
	for(n=0;n<4;n++)
	{
		if(step_event_count < Motors_Cnt[n]) 
		{
				step_event_count = Motors_Cnt[n];    //循环判断，获取各轴最大脉冲计数值
			  Max_axis = n;                          //记录脉冲数最多的轴号
		}
	}

	if(step_event_count == 0) { return; }   // 如果本次运动的最大脉冲计数=0，说明不需要运动。直接离开。
	
	switch(Max_axis)  //根据脉冲最多的轴，计算每轴对应的脉冲时间比例
	{
		case 0 : 
							Motor_time_ratio[M1] = 1.0;
							Motor_time_ratio[M2] = (float)(Motors_Cnt[M2]) / (float)(Motors_Cnt[M1]);      //计算电机的脉冲比例;
							Motor_time_ratio[M3] = (float)(Motors_Cnt[M3]) / (float)(Motors_Cnt[M1]);      //计算电机的脉冲比例;
							Motor_time_ratio[M4] = (float)(Motors_Cnt[M4]) / (float)(Motors_Cnt[M1]);      //计算电机的脉冲比例;
							MAX_STEPS_PER_MM = DEFAULT_M1_STEPS_PER_MM;
							break;
		case 1 : 
							Motor_time_ratio[M1] = (float)(Motors_Cnt[M1]) / (float)(Motors_Cnt[M2]);      //计算电机的脉冲比例;
							Motor_time_ratio[M2] = 1.0;      //计算两电机的脉冲比例;
							Motor_time_ratio[M3] = (float)(Motors_Cnt[M3]) / (float)(Motors_Cnt[M2]);      //计算电机的脉冲比例;
							Motor_time_ratio[M4] = (float)(Motors_Cnt[M4]) / (float)(Motors_Cnt[M2]);      //计算电机的脉冲比例;
							MAX_STEPS_PER_MM = DEFAULT_M2_STEPS_PER_MM;
							break;
		case 2 : 
							Motor_time_ratio[M1] = (float)(Motors_Cnt[M1]) / (float)(Motors_Cnt[M3]);      //计算电机的脉冲比例;
							Motor_time_ratio[M2] = (float)(Motors_Cnt[M2]) / (float)(Motors_Cnt[M3]);      //计算电机的脉冲比例;
							Motor_time_ratio[M3] = 1.0;      //计算两电机的脉冲比例;
							Motor_time_ratio[M4] = (float)(Motors_Cnt[M4]) / (float)(Motors_Cnt[M3]);      //计算电机的脉冲比例;
							MAX_STEPS_PER_MM = DEFAULT_M3_STEPS_PER_MM;
							break;
		case 3 : 
							Motor_time_ratio[M1] = (float)(Motors_Cnt[M1]) / (float)(Motors_Cnt[M4]);      //计算电机的脉冲比例;
							Motor_time_ratio[M2] = (float)(Motors_Cnt[M2]) / (float)(Motors_Cnt[M4]);      //计算电机的脉冲比例;
							Motor_time_ratio[M3] = (float)(Motors_Cnt[M3]) / (float)(Motors_Cnt[M4]);      //计算电机的脉冲比例;
							Motor_time_ratio[M4] = 1.0;      //计算两电机的脉冲比例;
							MAX_STEPS_PER_MM = DEFAULT_M4_STEPS_PER_MM;
							break;
	}
	
	
	
	
	Vn = Feed_Rate;
	S = step_event_count;
	S1 = (int)(((Feed_Rate*Feed_Rate*1.0 - V0*V0)*MAX_STEPS_PER_MM) / (MOTOR_ACCELERATION * 2));   //计算加速阶段脉冲数
	S3 = (int)(((Feed_Rate*Feed_Rate*1.0 - Vt*Vt)*MAX_STEPS_PER_MM) / (MOTOR_ACCELERATION * 2));   //计算减速阶段脉冲数
	S2 = S-S1-S3;                                                  //计算匀速阶段脉冲数
	
	
	UART_Send_string(1,"M1:");
	UART_Send_byte(1,(int)(Motors_Cnt[M1])/10000 +0x30);
	UART_Send_byte(1,(int)(Motors_Cnt[M1])/1000%10 +0x30);
	UART_Send_byte(1,(int)(Motors_Cnt[M1])/100%10 +0x30);
	UART_Send_byte(1,(int)(Motors_Cnt[M1])/10%10 +0x30);
	UART_Send_byte(1,(int)(Motors_Cnt[M1])/1%10 +0x30);
	UART_Send_string(1,"\r\n");
	UART_Send_string(1,"M2:");
	UART_Send_byte(1,(int)(Motors_Cnt[M2])/10000 +0x30);
	UART_Send_byte(1,(int)(Motors_Cnt[M2])/1000%10 +0x30);
	UART_Send_byte(1,(int)(Motors_Cnt[M2])/100%10 +0x30);
	UART_Send_byte(1,(int)(Motors_Cnt[M2])/10%10 +0x30);
	UART_Send_byte(1,(int)(Motors_Cnt[M2])/1%10 +0x30);
	UART_Send_string(1,"\r\n");
	UART_Send_string(1,"M3:");
	UART_Send_byte(1,(int)(Motors_Cnt[M3])/10000 +0x30);
	UART_Send_byte(1,(int)(Motors_Cnt[M3])/1000%10 +0x30);
	UART_Send_byte(1,(int)(Motors_Cnt[M3])/100%10 +0x30);
	UART_Send_byte(1,(int)(Motors_Cnt[M3])/10%10 +0x30);
	UART_Send_byte(1,(int)(Motors_Cnt[M3])/1%10 +0x30);
	UART_Send_string(1,"\r\n");
	UART_Send_string(1,"M4:");
	UART_Send_byte(1,(int)(Motors_Cnt[M4])/10000 +0x30);
	UART_Send_byte(1,(int)(Motors_Cnt[M4])/1000%10 +0x30);
	UART_Send_byte(1,(int)(Motors_Cnt[M4])/100%10 +0x30);
	UART_Send_byte(1,(int)(Motors_Cnt[M4])/10%10 +0x30);
	UART_Send_byte(1,(int)(Motors_Cnt[M4])/1%10 +0x30);
	UART_Send_string(1,"\r\n");

	/*UART_Send_string(1,"S01:");
	UART_Send_byte(1,(int)(S1)/10000 +0x30);
	UART_Send_byte(1,(int)(S1)/1000%10 +0x30);
	UART_Send_byte(1,(int)(S1)/100%10 +0x30);
	UART_Send_byte(1,(int)(S1)/10%10 +0x30);
	UART_Send_byte(1,(int)(S1)/1%10 +0x30);
	UART_Send_string(1,"\r\n");
	
	UART_Send_string(1,"S02:");
	UART_Send_byte(1,(int)(S2)/10000 +0x30);
	UART_Send_byte(1,(int)(S2)/1000%10 +0x30);
	UART_Send_byte(1,(int)(S2)/100%10 +0x30);
	UART_Send_byte(1,(int)(S2)/10%10 +0x30);
	UART_Send_byte(1,(int)(S2)/1%10 +0x30);
	UART_Send_string(1,"\r\n");
	
	UART_Send_string(1,"S03:");
	if(S3<0) UART_Send_byte(1,'-');
	UART_Send_byte(1,(int)(S3)/10000 +0x30);
	UART_Send_byte(1,(int)(S3)/1000%10 +0x30);
	UART_Send_byte(1,(int)(S3)/100%10 +0x30);
	UART_Send_byte(1,(int)(S3)/10%10 +0x30);
	UART_Send_byte(1,(int)(S3)/1%10 +0x30);
	UART_Send_string(1,"\r\n");*/
	
	
	if(S2<=0)      //无匀速阶段
	{
		Vm = sqrt(MOTOR_ACCELERATION*(S/MAX_STEPS_PER_MM) + V0*V0 + Vt*Vt);  //计算无匀速阶段的最大速度
		S1= S/2;
		//S1 = (int)(((2*MOTOR_ACCELERATION*S + Vt*Vt - V0*V0)*MAX_STEPS_PER_MM)/(4*MOTOR_ACCELERATION)); //从重新计算每个阶段的脉冲步数
		S2 = 0;
		S3 = S-S1;
		T1 = (Vm-V0)/MOTOR_ACCELERATION;
		T3=T1;
		//T1 = (float)((2*S1)/((V0+Vm)*MAX_STEPS_PER_MM));   //计算加速、匀速、减速的总时间
		T2 = 0.0;
		//T3 = (float)((2*S3)/((Vm+Vt)*MAX_STEPS_PER_MM));
		
		Vn=Vm;   //将Vm赋值给Vn
	}
	else           //有匀速阶段
	{
		T1 = (float)((2*S1)/((V0+Vn)*MAX_STEPS_PER_MM));	//计算加速、匀速、减速的总时间
		T2 = (float)(S2/(Vn*MAX_STEPS_PER_MM))*2; 
		T3 = (float)((2*S3)/((Vn+Vt)*MAX_STEPS_PER_MM));
	}
	
	/*UART_Send_string(1,"S1:");
	UART_Send_byte(1,(int)(S1)/10000 +0x30);
	UART_Send_byte(1,(int)(S1)/1000%10 +0x30);
	UART_Send_byte(1,(int)(S1)/100%10 +0x30);
	UART_Send_byte(1,(int)(S1)/10%10 +0x30);
	UART_Send_byte(1,(int)(S1)/1%10 +0x30);
	UART_Send_string(1,"\r\n");
	
	UART_Send_string(1,"S2:");
	UART_Send_byte(1,(int)(S2)/10000 +0x30);
	UART_Send_byte(1,(int)(S2)/1000%10 +0x30);
	UART_Send_byte(1,(int)(S2)/100%10 +0x30);
	UART_Send_byte(1,(int)(S2)/10%10 +0x30);
	UART_Send_byte(1,(int)(S2)/1%10 +0x30);
	UART_Send_string(1,"\r\n");
	
	UART_Send_string(1,"S3:");
	if(S3<0) UART_Send_byte(1,'-');
	UART_Send_byte(1,(int)(S3)/10000 +0x30);
	UART_Send_byte(1,(int)(S3)/1000%10 +0x30);
	UART_Send_byte(1,(int)(S3)/100%10 +0x30);
	UART_Send_byte(1,(int)(S3)/10%10 +0x30);
	UART_Send_byte(1,(int)(S3)/1%10 +0x30);
	UART_Send_string(1,"\r\n");*/
	
	UART_Send_string(1,"T1:");
	UART_Send_byte(1,(int)(T1*1000)/10000 +0x30);
	UART_Send_byte(1,(int)(T1*1000)/1000%10 +0x30);
	UART_Send_byte(1,(int)(T1*1000)/100%10 +0x30);
	UART_Send_byte(1,(int)(T1*1000)/10%10 +0x30);
	UART_Send_byte(1,(int)(T1*1000)/1%10 +0x30);
	UART_Send_string(1,"\r\n");
	
	UART_Send_string(1,"T2:");
	UART_Send_byte(1,(int)(T2*1000)/10000 +0x30);
	UART_Send_byte(1,(int)(T2*1000)/1000%10 +0x30);
	UART_Send_byte(1,(int)(T2*1000)/100%10 +0x30);
	UART_Send_byte(1,(int)(T2*1000)/10%10 +0x30);
	UART_Send_byte(1,(int)(T2*1000)/1%10 +0x30);
	UART_Send_string(1,"\r\n");
	
	UART_Send_string(1,"T3:");
	UART_Send_byte(1,(int)(T3*1000)/10000 +0x30);
	UART_Send_byte(1,(int)(T3*1000)/1000%10 +0x30);
	UART_Send_byte(1,(int)(T3*1000)/100%10 +0x30);
	UART_Send_byte(1,(int)(T3*1000)/10%10 +0x30);
	UART_Send_byte(1,(int)(T3*1000)/1%10 +0x30);
	UART_Send_string(1,"\r\n");
	
	
	K1 = (int)(Section * T1);       //计算各阶段分段步骤数
	K2 = 1;
	K3 = (int)(Section * T3);
	
	Start_Flag=0;
	m = 0;
	for(n=0;n<K1;n++)   //将加速阶段写进缓冲区
	{
		Planning_Step[Start_Flag] = (T1/K1)*MAX_STEPS_PER_MM*(V0+MOTOR_ACCELERATION*n * (T1/K1));  //计算脉冲数
		if(Planning_Step[Start_Flag] == 0 ) {Planning_Step[Start_Flag] = 1;}    //+1补偿避免出现0
		//Planning_Step[n] = (int)((((V0+MOTOR_ACCELERATION*T1*(n/K1)) + (V0+MOTOR_ACCELERATION*T1*((n+1)/K1)))*(T1/K1))/2); //计算加速阶段每个步骤脉冲数
		Planning_Time[Start_Flag] = (int)(T1*1000/K1);  //计算加速阶段每个步骤脉冲时间
		
		m+= Planning_Step[n];       //获取加速阶段脉冲和，因为存在浮点数运算，所以后续需要进行补偿
		
		Start_Flag += 1;     //规划缓冲区+1
	}

	n=2*(S1-m);
	//if(n<1) n=1;
	Planning_Step[Start_Flag] = S2+n;   //将匀速阶段写进缓冲区,并进行补偿
	Planning_Time[Start_Flag] = (int)(((S2+2*(S1-m))*1000.0)/(Vn*MAX_STEPS_PER_MM));  //计算加速阶段每个步骤脉冲时间
	
	m=Start_Flag;        //记录匀速阶段的段位置
	Start_Flag += 1;     //规划缓冲区+1
	
	
	for(n=0;n<K3;n++)   //对称将减速阶段写进缓冲区
	{
		Planning_Step[Start_Flag] = Planning_Step[m-(n+1)];
		//Planning_Step[Start_Flag] = (int)((((Vn-MOTOR_ACCELERATION*T3*(n/K3)) + (Vn-MOTOR_ACCELERATION*T3*((n+1)/K3)))*(T3/K3))/2); //计算加速阶段每个步骤脉冲数
		Planning_Time[Start_Flag] = (int)(T3*1000/K3);  //计算加速阶段每个步骤脉冲时间
		
		Start_Flag += 1;     //规划缓冲区+1
	}
	
//		Motors_Start(M1);  //电机使能
//		Motors_Start(M2);
//		Motors_Start(M3);
//		Motors_Start(M4);
		
		DIR1 = !Motors_Dir[M1]; //给电机运行方向
		DIR2 = !Motors_Dir[M2];
		DIR3 = Motors_Dir[M3];
		DIR4 = Motors_Dir[M4];
	
	
	
	Motors_Busy = 1;   //电机运行标志置位
	STEP_n=0;          //总脉冲计数变量清0
	ratio_n[M1]=0;
	ratio_n[M2]=0;
	ratio_n[M3]=0;
	ratio_n[M4]=0;
	
	m=0;
	for(n=0;n<Start_Flag;n++)   //串口发送加减速数据
	{
		m+=Planning_Step[n];
		
	/*	UART_Send_string(1,"STEP");
		UART_Send_byte(1,(int)(n)/100%10 +0x30);
		UART_Send_byte(1,(int)(n)/10%10 +0x30);
		UART_Send_byte(1,(int)(n)/1%10 +0x30);
		
		UART_Send_string(1,":");
		if(Planning_Step[n]<0) 
		{UART_Send_byte(1,'-');}
		
		UART_Send_byte(1,(int)abs(Planning_Step[n])/10000 +0x30);
		UART_Send_byte(1,(int)abs(Planning_Step[n])/1000%10 +0x30);
		UART_Send_byte(1,(int)abs(Planning_Step[n])/100%10 +0x30);
		UART_Send_byte(1,(int)abs(Planning_Step[n])/10%10 +0x30);
		UART_Send_byte(1,(int)abs(Planning_Step[n])/1%10 +0x30);
		
		UART_Send_string(1,"   T:");
		UART_Send_byte(1,(int)(Planning_Time[n])/10000 +0x30);
		UART_Send_byte(1,(int)(Planning_Time[n])/1000%10 +0x30);
		UART_Send_byte(1,(int)(Planning_Time[n])/100%10 +0x30);
		UART_Send_byte(1,(int)(Planning_Time[n])/10%10 +0x30);
		UART_Send_byte(1,(int)(Planning_Time[n])/1%10 +0x30);
		
		UART_Send_string(1,"\r\n");*/
	}
	UART_Send_string(1,"m:");
	UART_Send_byte(1,(int)(m)/10000 +0x30);
	UART_Send_byte(1,(int)(m)/1000%10 +0x30);
	UART_Send_byte(1,(int)(m)/100%10 +0x30);
	UART_Send_byte(1,(int)(m)/10%10 +0x30);
	UART_Send_byte(1,(int)(m)/1%10 +0x30);
	UART_Send_string(1,"\r\n");
	
	UART_Send_string(1,"K:");
	UART_Send_byte(1,(int)(K1+K2+K3)/10000 +0x30);
	UART_Send_byte(1,(int)(K1+K2+K3)/1000%10 +0x30);
	UART_Send_byte(1,(int)(K1+K2+K3)/100%10 +0x30);
	UART_Send_byte(1,(int)(K1+K2+K3)/10%10 +0x30);
	UART_Send_byte(1,(int)(K1+K2+K3)/1%10 +0x30);
	UART_Send_string(1,"\r\n");
	
	
}

void Motors_Init(void)
{
	
	Motors_Current[M1] = 0;  //每轴绝对位置清0
	Motors_Current[M2] = 0;
	Motors_Current[M3] = 0;
	Motors_Current[M4] = 0;
	
	M1_Current =0.0;    //每轴实际位置清0
	M2_Current =0.0;
	M3_Current =0.0;
	M1_Current =0.0;
	
	Motors_Fully = 0; 
	
	Start_Flag =0;
	End_Flag =0;
	
	Motors_Start(M1);  //电机使能
	Motors_Start(M2);
	Motors_Start(M3);
	Motors_Start(M4);
	
	delay_X_ms(100)	;
	O07 = 0;	  //松开抱闸
		
}
void Run_Moto(void)
{
	uint Time_i;
	
	if(Motors_Busy == 1)  //电机运行标志
	{
		//BUZZ = 0;
		if(End_Flag >= Start_Flag && End_Flag != 0)  //缓冲区头=缓冲区尾  说明一次运动执行完成
			{
				Motors_Busy = 0;  //电机休息，表示运行结束
				End_Flag =0;      //复位缓冲区
				Start_Flag = 0;
				
			//	BUZZ = 1;
//				Motors_Stop(M1);   //电机失能
//				Motors_Stop(M2);
//				Motors_Stop(M3);
//				Motors_Stop(M4);
				
				while(ratio_n[M1]<Motors_Cnt[M1]*2)  //补偿float计算脉冲生成误差
				{
					PULSE1 = !PULSE1;    //反转电平
					ratio_n[M1]+=1;
				}
				while(ratio_n[M2]<Motors_Cnt[M2]*2)  //补偿float计算脉冲生成误差
				{
					PULSE2 = !PULSE2;    //反转电平
					ratio_n[M2]+=1;
				}
				while(ratio_n[M3]<Motors_Cnt[M3]*2)  //补偿float计算脉冲生成误差
				{
					PULSE3 = !PULSE3;    //反转电平
					ratio_n[M3]+=1;
				}
				while(ratio_n[M4]<Motors_Cnt[M4]*2)  //补偿float计算脉冲生成误差
				{
					PULSE4 = !PULSE4;    //反转电平
					ratio_n[M4]+=1;
				}
				
				Motors_Current[M1] = Motors_target[M1]; //将当前位置赋值给绝对脉冲数
				Motors_Current[M2] = Motors_target[M2];
				Motors_Current[M3] = Motors_target[M3];
				Motors_Current[M4] = Motors_target[M4];
				
				M1_Current = Motors_Current[M1]/DEFAULT_M1_STEPS_PER_MM;	//将当前位置赋值给绝对位置
				M2_Current = Motors_Current[M2]/DEFAULT_M2_STEPS_PER_MM;
				M3_Current = Motors_Current[M3]/DEFAULT_M3_STEPS_PER_MM;
				M4_Current = Motors_Current[M4]/DEFAULT_M4_STEPS_PER_MM;
				
				M_num=0;
				/*
				UART_Send_string(1,"P:");
				UART_Send_byte(1,(int)(M_num/2)/10000 +0x30);
				UART_Send_byte(1,(int)(M_num/2)/1000%10 +0x30);
				UART_Send_byte(1,(int)(M_num/2)/100%10 +0x30);
				UART_Send_byte(1,(int)(M_num/2)/10%10 +0x30);
				UART_Send_byte(1,(int)(M_num/2)/1%10 +0x30);
				UART_Send_string(1,"\r\n");
				
				
				UART_Send_string(1,"M1o:");
				UART_Send_byte(1,(int)(ratio_n[M1]/2)/10000 +0x30);
				UART_Send_byte(1,(int)(ratio_n[M1]/2)/1000%10 +0x30);
				UART_Send_byte(1,(int)(ratio_n[M1]/2)/100%10 +0x30);
				UART_Send_byte(1,(int)(ratio_n[M1]/2)/10%10 +0x30);
				UART_Send_byte(1,(int)(ratio_n[M1]/2)/1%10 +0x30);
				UART_Send_string(1,"\r\n");
				UART_Send_string(1,"M2o:");
				UART_Send_byte(1,(int)(ratio_n[M2]/2)/10000 +0x30);
				UART_Send_byte(1,(int)(ratio_n[M2]/2)/1000%10 +0x30);
				UART_Send_byte(1,(int)(ratio_n[M2]/2)/100%10 +0x30);
				UART_Send_byte(1,(int)(ratio_n[M2]/2)/10%10 +0x30);
				UART_Send_byte(1,(int)(ratio_n[M2]/2)/1%10 +0x30);
				UART_Send_string(1,"\r\n");
				UART_Send_string(1,"M3o:");
				UART_Send_byte(1,(int)(ratio_n[M3]/2)/10000 +0x30);
				UART_Send_byte(1,(int)(ratio_n[M3]/2)/1000%10 +0x30);
				UART_Send_byte(1,(int)(ratio_n[M3]/2)/100%10 +0x30);
				UART_Send_byte(1,(int)(ratio_n[M3]/2)/10%10 +0x30);
				UART_Send_byte(1,(int)(ratio_n[M3]/2)/1%10 +0x30);
				UART_Send_string(1,"\r\n");
				UART_Send_string(1,"M4o:");
				UART_Send_byte(1,(int)(ratio_n[M4]/2)/10000 +0x30);
				UART_Send_byte(1,(int)(ratio_n[M4]/2)/1000%10 +0x30);
				UART_Send_byte(1,(int)(ratio_n[M4]/2)/100%10 +0x30);
				UART_Send_byte(1,(int)(ratio_n[M4]/2)/10%10 +0x30);
				UART_Send_byte(1,(int)(ratio_n[M4]/2)/1%10 +0x30);
				UART_Send_string(1,"\r\n");
				
				UART_Send_string(1,"M1c:");
				UART_Send_byte(1,(int)(Motors_Current[M1])/10000 +0x30);
				UART_Send_byte(1,(int)(Motors_Current[M1])/1000%10 +0x30);
				UART_Send_byte(1,(int)(Motors_Current[M1])/100%10 +0x30);
				UART_Send_byte(1,(int)(Motors_Current[M1])/10%10 +0x30);
				UART_Send_byte(1,(int)(Motors_Current[M1])/1%10 +0x30);
				UART_Send_string(1,"\r\n");
				UART_Send_string(1,"M2c:");
				UART_Send_byte(1,(int)(Motors_Current[M2])/10000 +0x30);
				UART_Send_byte(1,(int)(Motors_Current[M2])/1000%10 +0x30);
				UART_Send_byte(1,(int)(Motors_Current[M2])/100%10 +0x30);
				UART_Send_byte(1,(int)(Motors_Current[M2])/10%10 +0x30);
				UART_Send_byte(1,(int)(Motors_Current[M2])/1%10 +0x30);
				UART_Send_string(1,"\r\n");
				UART_Send_string(1,"M3c:");
				UART_Send_byte(1,(int)(Motors_Current[M3])/10000 +0x30);
				UART_Send_byte(1,(int)(Motors_Current[M3])/1000%10 +0x30);
				UART_Send_byte(1,(int)(Motors_Current[M3])/100%10 +0x30);
				UART_Send_byte(1,(int)(Motors_Current[M3])/10%10 +0x30);
				UART_Send_byte(1,(int)(Motors_Current[M3])/1%10 +0x30);
				UART_Send_string(1,"\r\n");
				UART_Send_string(1,"M4c:");
				UART_Send_byte(1,(int)(Motors_Current[M4])/10000 +0x30);
				UART_Send_byte(1,(int)(Motors_Current[M4])/1000%10 +0x30);
				UART_Send_byte(1,(int)(Motors_Current[M4])/100%10 +0x30);
				UART_Send_byte(1,(int)(Motors_Current[M4])/10%10 +0x30);
				UART_Send_byte(1,(int)(Motors_Current[M4])/1%10 +0x30);
				UART_Send_string(1,"\r\n");
				*/
				UART_Send_string(1,"END！\r\n\r\n");
				
				return;
			}
			
		if(End_Flag<Start_Flag && Start_Flag != 0 && Motors_Fully == 0) //如果缓冲区还有数据，并且上一次缓冲执行已完成
		{
			
			Time_i = (int)(((float)(Planning_Time[End_Flag]*500.0)/Planning_Step[End_Flag]));  //计算单次定时中断的时长，单位US
			
			Time0_step = Planning_Step[End_Flag];       //本次要生成的脉冲数
			
			Motors_Fully=1;      //电机忙标志
			
//			UART_Send_string(1,"T:");
//			UART_Send_byte(1,(int)(End_Flag)/10000 +0x30);
//			UART_Send_byte(1,(int)(End_Flag)/1000%10 +0x30);
//			UART_Send_byte(1,(int)(End_Flag)/100%10 +0x30);
//			UART_Send_byte(1,(int)(End_Flag)/10%10 +0x30);
//			UART_Send_byte(1,(int)(End_Flag)/1%10 +0x30);
//			UART_Send_string(1,"\r\n");
			
			
			T0_n=0;
			
			
			
			if(Time_i<2000)
			{
				PIT_init_us(0,Time_i);  //打开定时器中断
			}
			else
			{
				PIT_init_ms(0,Time_i/1000);  //打开定时器中断
			}
		}
		
		
	}
}

void TM0_isr(void)   //产生脉冲的定时器
{
	
	
	if(T0_n < 2*Time0_step )  //两次反转生成一个脉冲
	{
		T0_n+=1;
		
		if((int)((STEP_n+T0_n) * Motor_time_ratio[M1]) != ratio_n[M1])
		{
			M_num +=1;
			PULSE1 = !PULSE1;    //反转电平
			ratio_n[M1] = (int)((STEP_n+T0_n) * Motor_time_ratio[M1]);
		}
		if((int)((STEP_n+T0_n) * Motor_time_ratio[M2]) != ratio_n[M2])
		{
			PULSE2 = !PULSE2;    //反转电平
			ratio_n[M2] = (int)((STEP_n+T0_n) * Motor_time_ratio[M2]);
		}
		if((int)((STEP_n+T0_n) * Motor_time_ratio[M3]) != ratio_n[M3])
		{
			PULSE3 = !PULSE3;    //反转电平
			ratio_n[M3] = (int)((STEP_n+T0_n) * Motor_time_ratio[M3]);
		}
		if((int)((STEP_n+T0_n) * Motor_time_ratio[M4]) != ratio_n[M4])
		{
			PULSE4 = !PULSE4;    //反转电平
			ratio_n[M4] = (int)((STEP_n+T0_n) * Motor_time_ratio[M4]);
		}
	}
	else               //本次脉冲生成完成
	{
		STEP_n+=T0_n;
		
		End_Flag+=1;   //缓冲区尾向前递进+1
		T0_n=0;
		Motors_Fully=0;  //电机忙标志
		ET0 = 0;  //关闭定时器
		TR0 = 0;  //定时器停止
		Run_Moto(); //进行下一次计算
	}
}

void Motors_Start(unsigned char Chs)  //电机使能
{
	switch(Chs)
	{
		case 0: EN1=0;break;
		case 1: EN2=0;break;
		case 2: EN3=0;break;
		case 3: EN4=0;break;
	}
}

void Motors_Stop(unsigned char Chs)  //电机失能
{
	switch(Chs)
	{
		case 0: EN1=1;break;
		case 1: EN2=1;break;
		case 2: EN3=1;break;
		case 3: EN4=1;break;
	}
}