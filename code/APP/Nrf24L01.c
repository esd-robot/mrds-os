/********************************************
*********************************************************/
#include "Nrf24L01.h"
sbit NRF_MOSI = P4^0;
sbit NRF_SCK = P4^3;
sbit NRF_MISO = P4^1;
sbit NRF_CSN = P5^4;
sbit NRF_CE = P4^2;
sbit NRF_IRQ = P3^3;
/**********  NRF24L01寄存器操作命令  ***********/
#define READ_REG        0x00  //读寄存器地址，低5位为寄存器地址
#define WRITE_REG       0x20  //写寄存器地址，低5位为寄存器地?
#define RD_RX_PLOAD     0x61  //读RX有效数据,1~32字节
#define WR_TX_PLOAD     0xA0  //写TX有效数据,1~32字节
#define FLUSH_TX        0xE1  //清除TX FIFO寄存器.发射模式下用
#define FLUSH_RX        0xE2  //清除RX FIFO寄存器.接收模式下用
#define REUSE_TX_PL     0xE3  //重新使用上一包数据,CE为高,数据包被不断发送.
#define _2401NOP             0xFF  //空操作，用来读取状态寄存器	

/**********  NRF24L01寄存器地址   *************/
#define CONFIG          0x00  //配置寄存器地址                             
#define EN_AA           0x01  //使能自动应答功能
#define EN_RXADDR       0x02  //接收地址允许
#define SETUP_AW        0x03  //设置地址宽度(所有数据通道)
#define SETUP_RETR      0x04  //建立自动重发
#define RF_CH           0x05  //RF通道
#define RF_SETUP        0x06  //RF寄存器
#define STATUS          0x07  //状态寄存器
#define OBSERVE_TX      0x08  // 发送检测寄存器
#define CD              0x09  // 载波检测寄存器
#define RX_ADDR_P0      0x0A  // 数据通道0接收地址
#define RX_ADDR_P1      0x0B  // 数据通道1接收地址
#define RX_ADDR_P2      0x0C  // 数据通道2接收地址
#define RX_ADDR_P3      0x0D  // 数据通道3接收地址
#define RX_ADDR_P4      0x0E  // 数据通道4接收地址
#define RX_ADDR_P5      0x0F  // 数据通道5接收地址
#define TX_ADDR         0x10  // 发送地址寄存器
#define RX_PW_P0        0x11  // 接收数据通道0有效数据宽度(1~32字节) 
#define RX_PW_P1        0x12  // 接收数据通道1有效数据宽度(1~32字节) 
#define RX_PW_P2        0x13  // 接收数据通道2有效数据宽度(1~32字节) 
#define RX_PW_P3        0x14  // 接收数据通道3有效数据宽度(1~32字节)  
#define RX_PW_P4        0x15  // 接收数据通道4有效数据宽度(1~32字节) 
#define RX_PW_P5        0x16  // 接收数据通道5有效数据宽度(1~32字节) 
#define FIFO_STATUS     0x17  // FIFO状态寄存器
#define FEATURE         0x1D  // 功能寄存器
/*------------------------------------------------------------------*/

/******   STATUS寄存器bit位定义      *******/
#define MAX_TX  	0x10  	  //达到最大重发次数中断
#define TX_OK   	0x20  	  //TX发送完成中断
#define RX_OK   	0x40  	  //接收数据中断
/*------------------------------------------------------------------*/

/*********     24L01发送接收数据宽度定义	  ***********/
#define TX_ADR_WIDTH    5     //5字节地址宽度
#define RX_ADR_WIDTH    5     //5字节地址宽度
#define TX_PLOAD_WIDTH  32    //32字节有效数据宽度
#define RX_PLOAD_WIDTH  32    //32字节有效数据宽度


uchar TX_ADDRESS[TX_ADR_WIDTH]={0xFF,0xFF,0xFF,0xFF,0xFF}; //默认发送地址
uchar RX_ADDRESS[RX_ADR_WIDTH]={0xFF,0xFF,0xFF,0xFF,0xFF}; //默认接收地址

//uchar rece_buf[32];    			//接收发送数据的存储数组


//写数据
uchar SPI_RW(uchar byte0)
{
	#if HARD_SPI_2401
	SPDAT = byte0;					//DATA寄存器赋值
	while (!(SPSTAT & 0x80));  		//查询完成标志
	SPSTAT = 0xc0;                  //清中断标志
	return SPDAT;	
	#else
	uchar bit_ctr;
	for(bit_ctr=0;bit_ctr<8;bit_ctr++)  // ??8?
	{
		NRF_MOSI=(byte0&0x80); 			// MSB TO MOSI
		byte0=(byte0<<1);					// shift next bit to MSB
		NRF_SCK=1;
		byte0|=NRF_MISO;	        		// capture current MISO bit
		NRF_SCK=0;
	}
	return byte0;
	#endif
}
//写寄存器
uchar NRF24L01_Write_Reg(uchar reg,uchar value)
{
	uchar status;

	NRF_CSN=0;                  //CSN=0;   
  	status = SPI_RW(reg);		//???????,??????
	SPI_RW(value);
	NRF_CSN=1;                  //CSN=1;

	return status;
}
//写寄存器（多字节）
uchar NRF24L01_Write_Buf(uchar reg, uchar *pBuf, uchar len)
{
	uchar status,u8_ctr;
	NRF_CSN=0;
  	status = SPI_RW(reg);			//发送寄存器值(位置),并读取状态值
  	for(u8_ctr=0; u8_ctr<len; u8_ctr++)
	SPI_RW(*pBuf++); 				//写入数据
	NRF_CSN=1;
  	return status;          		//返回读到的状态值
}							  					   

//读寄存器
uchar NRF24L01_Read_Reg(uchar reg)
{
 	uchar value;

	NRF_CSN=0;              //CSN=0;   
  	SPI_RW(reg);			//??????(??),??????
	value = SPI_RW(_2401NOP);
	NRF_CSN=1;             	//CSN=1;

	return value;
}
//读寄存器（多字节）
uchar NRF24L01_Read_Buf(uchar reg,uchar *pBuf,uchar len)
{
	uchar status,u8_ctr;
	NRF_CSN=0;                   	//CSN=0       
  	status=SPI_RW(reg);				//???????,??????   	   
 	for(u8_ctr=0;u8_ctr<len;u8_ctr++)
	pBuf[u8_ctr]=SPI_RW(0XFF);		//????
	NRF_CSN=1;                 		//CSN=1
  	return status;        			//????????
}
//24L01接收数据
uchar NRF24L01_Get_Data(uchar *rxbuf)
{
	uchar state;
	 
	state=NRF24L01_Read_Reg(STATUS);  			//?????????    	 
	NRF24L01_Write_Reg(WRITE_REG+STATUS,state); //??TX_DS?MAX_RT????
	if(state&RX_OK)								//?????
	{
		NRF_CE = 0;
		NRF24L01_Read_Buf(RD_RX_PLOAD,rxbuf,RX_PLOAD_WIDTH);//????
		NRF24L01_Write_Reg(FLUSH_RX,0xff);					//??RX FIFO???
		NRF_CE = 1;
		delay_150us(); 
		return 0; 
	}	   
	return 1;//???????
}
//设置24L01发送模数
uchar NRF24L01_TxPacket(uchar *txbuf)
{
	uchar state;
   
	NRF_CE=0;												//CE??,??24L01??
  	NRF24L01_Write_Buf(WR_TX_PLOAD,txbuf,TX_PLOAD_WIDTH);	//????TX BUF  32???
 	NRF_CE=1;												//CE??,????	   
	while(NRF_IRQ==1);										//??????
	state=NRF24L01_Read_Reg(STATUS);  						//?????????	   
	NRF24L01_Write_Reg(WRITE_REG+STATUS,state); 			//??TX_DS?MAX_RT????
	if(state&MAX_TX)										//????????
	{
		NRF24L01_Write_Reg(FLUSH_TX,0xff);					//??TX FIFO??? 
		return MAX_TX; 
	}
	if(state&TX_OK)											//????
	{
		return TX_OK;
	}
	return 0xff;											//????
}
//检测24L01是否存在
uchar NRF24L01_Check(void)
{
	uchar check_in_buf[5]={0x11,0x22,0x33,0x44,0x55};
	uchar check_out_buf[5]={0x00};

	NRF_SCK=0;
	NRF_CSN=1;    
	NRF_CE=0;

	NRF24L01_Write_Buf(WRITE_REG+TX_ADDR, check_in_buf, 5);

	NRF24L01_Read_Buf(READ_REG+TX_ADDR, check_out_buf, 5);

	if((check_out_buf[0] == 0x11)&&\
	   (check_out_buf[1] == 0x22)&&\
	   (check_out_buf[2] == 0x33)&&\
	   (check_out_buf[3] == 0x44)&&\
	   (check_out_buf[4] == 0x55))return 0;
	else return 1;
}	

void NRF24L01_Init(uchar Freq ,uint RXADDR ,uint TXADDR,uchar MODE) //24L01初始化函数
{	
		TX_ADDRESS[3] = TXADDR/256;  //获取发送地址
		TX_ADDRESS[4] = TXADDR%256;  //获取发送地址
		RX_ADDRESS[3] = RXADDR/256;  //获取接收地址
		RX_ADDRESS[4] = RXADDR%256;  //获取接收地址
	
		while(NRF24L01_Check());                    // 检测NRF24L01是否存在
	
		NRF_CE=0;	
		NRF24L01_Write_Reg(WRITE_REG+STATUS,0xff);//????0???????	
	
		NRF24L01_Write_Reg(WRITE_REG+FEATURE,0x01);
  	NRF24L01_Write_Reg(WRITE_REG+RX_PW_P0,RX_PLOAD_WIDTH);//????0???????
	  NRF24L01_Write_Reg(FLUSH_RX,0xff);									//??RX FIFO???    
  	NRF24L01_Write_Buf(WRITE_REG+TX_ADDR,(uchar*)TX_ADDRESS,TX_ADR_WIDTH);//?TX???? 
  	NRF24L01_Write_Buf(WRITE_REG+RX_ADDR_P0,(uchar*)RX_ADDRESS,RX_ADR_WIDTH); //??TX????,??????ACK	  
  	NRF24L01_Write_Reg(WRITE_REG+EN_AA,0x01);       //数据通道1使能自动应答  
  	NRF24L01_Write_Reg(WRITE_REG+EN_RXADDR,0x01);   //接收通道地址1使能
  	NRF24L01_Write_Reg(WRITE_REG+SETUP_RETR,0x14);	//自动重发间隔:500us + 86us;自动重发次数:4次
  	NRF24L01_Write_Reg(WRITE_REG+RF_CH,Freq);         //通信频率：2.400GHz =2.4+xx*0.001GHz
  	NRF24L01_Write_Reg(WRITE_REG+RF_SETUP,0x23);    //发送功率,0db,1Mbps,空中传输速度   1M 0x03 2M 0x07 250K 0X23
		if(MODE == 0)	NRF24L01_Write_Reg(WRITE_REG+CONFIG,0x0f);      //接收模式
  	else NRF24L01_Write_Reg(WRITE_REG+CONFIG,0x0e);      //发送模式
	
		NRF_CE=1;									  //CE??,????
}

void NRF24L01_Send_Data(uchar *buf)
{
	NRF_CE=0;
	NRF24L01_Write_Reg(WRITE_REG+CONFIG,0x0e);  //设置发送模式
	NRF_CE=1;
	Delay_X_uS(15);
	NRF24L01_TxPacket(buf);
	NRF_CE=0;
	NRF24L01_Write_Reg(WRITE_REG+CONFIG, 0x0f);  //设置接收模式
	NRF_CE=1;	
}

/***************************************************
函数名称：延时函数
输入参数：ms = 延时的时长
函数功能：延时
***************************************************/


void delay_150us()
{
	unsigned long edata i;

	_nop_();
	_nop_();
	_nop_();
	i = 898UL;
	while (i) i--;
}



