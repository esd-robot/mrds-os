/*
2007-2-1 12:06
nokia 5110 driver program for 51 mcu
by zl0801

zhaoliang0801@gmail.com

*/

#include "STC8H.h"

// pin define for n5110lcd_8key board
// change this if your hardware changed!

sbit SCLK = P5^4;		// pin 2	 header	5
sbit SDIN = P3^4;		// pin 3	 header	4
sbit LCD_DC = P3^5;		// pin 4	 header	3
sbit LCD_CE = P3^6;		// pin 5	 header	2
sbit LCD_RST = P3^7;	// pin 9	 header	1


void LCD_init(void);   //5110液晶初始化函数
void LCD_clear(void);  //5110清屏函数
void LCD_move_chinese_string(unsigned char X, unsigned char Y, unsigned char T);  //显示汉字函数
void LCD_write_english_string(unsigned char X,unsigned char Y,char *s);  //英文字符串显示函数
void LCD_write_chinese_string(unsigned char X, unsigned char Y,
                   unsigned char ch_with,unsigned char num,
                   unsigned char line,unsigned char row);			//显示汉字函数
void chinese_string(unsigned char X, unsigned char Y, unsigned char T);                 
void LCD_write_char(unsigned char c);     //显示英文字符
void LCD_draw_bmp_pixel(unsigned char X,unsigned char Y,unsigned char *map,
                  unsigned char Pix_x,unsigned char Pix_y);    //显示图片函数
void LCD_write_byte(unsigned char dat, unsigned char dc);      //使用SPI接口写数据到LCD
void LCD_set_XY(unsigned char X, unsigned char Y);             //设置书写位置函数
void delay_1us(void);               //延时1us函数     
void LCD_PutLine(unsigned char X, unsigned char Y,unsigned char Dat);