#ifndef		__OLED_H
#define		__OLED_H
#include "config.h"
sbit OLED_SCL = P1^5;				//IIC屏幕SCL引脚
sbit OLED_SDIN = P1^4;			//IIC屏幕SDA引脚

#define OLED_CMD  0	//写命令
#define OLED_DATA 1	//写数据
#define OLED_MODE 0

#define OLED_SCLK_Clr() OLED_SCL=0
#define OLED_SCLK_Set() OLED_SCL=1

#define OLED_SDIN_Clr() OLED_SDIN=0
#define OLED_SDIN_Set() OLED_SDIN=1

//OLED模式设置
//0:4线串行模式
//1:并行8080模式

#define SIZE 16
#define XLevelL		0x02
#define XLevelH		0x10
#define Max_Column	128
#define Max_Row		64
#define	Brightness	0xFF 
#define X_WIDTH 	128
#define Y_WIDTH 	64	    						  
//-----------------OLED端口定义----------------  					   


//OLED控制用函数
void OLED_WR_Byte(unsigned dat,unsigned cmd);  
void OLED_Display_On(void);
void OLED_Display_Off(void);	   							   		    
void OLED_Init(void);   //OLED初始化函数
void OLED_Clear(void);
void OLED_DrawPoint(u8 x,u8 y,u8 t);
void OLED_Fill(u8 x1,u8 y1,u8 x2,u8 y2,u8 dot);   
void OLED_ShowChar(u8 x,u8 y,u8 chr,u8 Char_Size);   //OLED在特定位置显示一个字符
void OLED_ShowNum(u8 x,u8 y,u32 num,u8 len,u8 size);  //OLED在特定位置显示一个数字（无符号）
void OLED_ShowString(u8 x,u8 y, u8 *p,u8 Char_Size); //在特定位置显示一个字符串
void OLED_Set_Pos(unsigned char x, unsigned char y);  //设定要显示的位置
void OLED_ShowCHinese(u8 x,u8 y,u8 no);    //显示一个汉字
void OLED_DrawBMP(unsigned char x0, unsigned char y0,unsigned char x1, unsigned char y1,unsigned char BMP[]);//显示一副图片
void Delay_50ms(unsigned int Del_50ms);
void Delay_1ms(unsigned int Del_1ms);
void fill_picture(unsigned char fill_Data);
void Picture();
void IIC_Start();
void IIC_Stop();
void Write_IIC_Command(unsigned char IIC_Command);
void Write_IIC_Data(unsigned char IIC_Data);
void Write_IIC_Byte(unsigned char IIC_Byte);
void IIC_Wait_Ack();
void OLED_ShowFloat(uint16 x,uint16 y,double dat,uint8 num,uint8 pointnum,uint8 size2);  //显示一个浮点型变量
void OLED_ShowInt(uint16 x,uint16 y,int32 dat,uint8 num,uint8 size2);  //显示一个整型变量（有符号）
#endif