
#include "config.h"

									
void Init_DS3231(uchar year,uchar moon,uchar day,uchar week,uchar hour,uchar min,uchar sec);  //初始化时间
void IIC_Read_DS3231(void);     //读取时间

extern uchar DS_Year,DS_Moon,DS_Day,DS_Week,DS_Hour,DS_Min,DS_Sec,DS_Temp;  //时间缓存