#ifndef __NRF24L01_H
#define __NRF24L01_H
#include "config.h"
#define HARD_SPI_2401   (1)  //给成1使用硬件SPI  	给0使用软件SPI
extern uchar SPI_RW(uchar byte0);
extern uchar NRF24L01_Write_Reg(uchar reg,uchar value);
extern uchar NRF24L01_Read_Reg(uchar reg);
extern uchar NRF24L01_Read_Buf(uchar reg,uchar *pBuf,uchar len);
extern uchar NRF24L01_Write_Buf(uchar reg, uchar *pBuf, uchar len);
extern uchar NRF24L01_Get_Data(uchar *rxbuf);   //接收数据函数
extern uchar NRF24L01_TxPacket(uchar *txbuf);
extern uchar NRF24L01_Check(void);
extern void NRF24L01_Init(uchar Freq ,uint RXADDR ,uint TXADDR,uchar MODE); //初始化函数
extern void NRF24L01_Send_Data(uchar *buf);  //发送数据函数
extern void delay_us(uchar num);
extern void delay_150us();

#endif