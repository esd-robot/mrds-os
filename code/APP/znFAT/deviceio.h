#ifndef _DEVICE_IO_H_
#define _DEVICE_IO_H_

/****************《51单片机轻松入门-基于STC15W4K系列》配套例程 *************
 ★★★★★★★★★★★★★★★★★★★★★★★★
 《51单片机轻松入门-基于STC15W4K系列》 一书已经由北航出版社正式出版发行。
  作者亲手创作的与教材配套的51双核实验板(2个MCU)对程序下载、调试、仿真方便，不需要外部
  仿真器与编程器，这种设计方式彻底解决了系统中多个最高优先级谁也不能让谁的中断竞争问题。
  淘宝店地址：https://shop117387413.taobao.com
  QQ群：STC51-STM32(3) ：515624099 或 STC51-STM32(2)：99794374。
        验证信息：STC15单片机
  邮箱：xgliyouquan@126.com
  ★★★★★★★★★★★★★★★★★★★★★★★★*/

struct znFAT_IO_Ctl //底层驱动接口的IO频度控制体 
{
 UINT32 just_sec;
 UINT8  just_dev;
};

UINT8 znFAT_Device_Init(void);
UINT8 znFAT_Device_Read_Sector(UINT32 addr,UINT8 *buffer);
UINT8 znFAT_Device_Write_Sector(UINT32 addr,UINT8 *buffer);
UINT8 znFAT_Device_Read_nSector(UINT32 nsec,UINT32 addr,UINT8 *buffer);
UINT8 znFAT_Device_Write_nSector(UINT32 nsec,UINT32 addr,UINT8 *buffer);
UINT8 znFAT_Device_Clear_nSector(UINT32 nsec,UINT32 addr);

#endif