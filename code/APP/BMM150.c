/********************************************
*********************************************************/
#include "BMM150.h"
#include "config.h"          //通用配置头文件
/**********  BMM150寄存器操作命令  ***********/
#define READ_REG        0x80  //写操作
#define NOP             0xFF  //空操作，用来读取状态寄存器	

#define SPD_30HZ        0x38  //30HZ速度
#define SPD_25HZ        0x30  //25HZ速度
#define SPD_20HZ        0x21  //20HZ速度
#define SPD_15HZ        0x20  //15HZ速度
#define SPD_10HZ        0x00  //10HZ速度
#define SPD_6HZ         0x10  //6HZ速度

/**********  BMM150寄存器地址   *************/
#define CONFIG          0x4B  //电源和操作寄存器地址                             
#define Chip_ID         0x40  //获取ID寄存器地址
#define SPEED           0x4C  //速度控制寄存器
#define X_L_DATA        0x42  //X低位数据
#define X_H_DATA        0x43  //X高位数据
#define Y_L_DATA        0x44  //Y低位数据
#define Y_H_DATA        0x45  //Y高位数据
#define Z_L_DATA        0x46  //Z低位数据
#define Z_H_DATA        0x47  //Z高位数据


uint BMM150_XYZ[3]={0};


//写数据
uchar BMM150_SPI_RW(uchar BMMbyte0)
{
	uchar bit_ctr;
	//GPIO_init_pin(66,3);
	//BMM150_delay_us(10);
	for(bit_ctr=0;bit_ctr<8;bit_ctr++)  // ??8?
	{
		BMM150_MOSI=(BMMbyte0&0x80); 			// MSB TO MOSI
		BMMbyte0=(BMMbyte0<<1);					// shift next bit to MSB
		//BMM150_delay_us(1);
		BMM150_SCK=1;
		BMM150_delay_us(1);
		BMMbyte0|=BMM150_MISO;	        		// capture current MISO bit
		
		BMM150_SCK=0;
		//BMM150_delay_us(1);
	}
	//GPIO_init_pin(66,0);
	return BMMbyte0;
}
//写寄存器
uchar BMM150_Write_Reg(uchar reg,uchar value)
{
	uchar status;
		
	BMM150_CSN=1;
	BMM150_delay_us(1);
	BMM150_CSN=0;                  //CSN=0;   
	BMM150_delay_us(2);
  status = BMM150_SPI_RW(reg);		//???????,??????
	BMM150_delay_us(1);
	BMM150_SPI_RW(value);
	BMM150_CSN=1;                  //CSN=1;
  //BMM150_delay_us(15);
	return status;
}
						  					   
//读寄存器
uchar BMM150_Read_Reg(uchar reg)
{
 	uchar value;
	
  BMM150_CSN=1;
  BMM150_delay_us(1);
	BMM150_CSN=0;              //CSN=0;  
  BMM150_delay_us(1);	
  BMM150_SPI_RW(reg);			//??????(??),??????
	BMM150_delay_us(1);
	value = BMM150_SPI_RW(NOP);
	BMM150_CSN=1;             	//CSN=1;
  //BMM150_delay_us(15);
	return value;
}

void BMM150_Init(void) //初始化函数
{	
	uchar BMM150_ID=0;
	
	BMM150_Write_Reg(0x4b,0x01);
	BMM150_Write_Reg(0x4b,0x01);
	BMM150_Write_Reg(0X4C,0X06);    //测量速度30HZ
	BMM150_Write_Reg(0X4C,0X06);    //测量速度30HZ
	//BMM150_Write_Reg(0x4b,0x01);
  //delay_X_ms(1);
	BMM150_ID=BMM150_Read_Reg(0x40|READ_REG);
	UART_Send_byte(1,'I');
	UART_Send_byte(1,'D');
	UART_Send_byte(1,'=');
	UART_Send_byte(1,BMM150_ID/100%10+0x30);
	UART_Send_byte(1,BMM150_ID/10%10+0x30);
	UART_Send_byte(1,BMM150_ID/1%10+0x30);
	UART_Send_byte(1,'\r');
	UART_Send_byte(1,'\n');

	//BMM150_Write_Reg(0x4b,0x00);
	//BMM150_Write_Reg(0x4b,0x00);
	BMM150_Write_Reg(0x4C,0x00);    //测量速度30HZ
	BMM150_Write_Reg(0x4C,0x00);    //测量速度30HZ
}

void BMM150_Get_Data(void)
{
  uint Byte1,Byte2;
	
	//BMM150_Single_WriteI2C(SPEED,SPD_30HZ);
	//BMM150_Write_Reg(0x4B,0x01);    //测量速度30HZ
	//BMM150_Write_Reg(0x4C,0x38);    //测量速度30HZ
	//BMM150_Write_Reg(0x4C,0x38);    //测量速度30HZ
	
	Byte1 = BMM150_Read_Reg(X_L_DATA|READ_REG);
	Byte1 = BMM150_Read_Reg(X_L_DATA|READ_REG);
	Byte2 = BMM150_Read_Reg(X_H_DATA|READ_REG);
	Byte2 = BMM150_Read_Reg(X_H_DATA|READ_REG);
	BMM150_XYZ[0]=(Byte1<<8+(Byte2&0xf8))>>3;
	BMM150_XYZ[0]=Byte2;
	Byte1 = BMM150_Read_Reg(Y_L_DATA|READ_REG);
	Byte1 = BMM150_Read_Reg(Y_L_DATA|READ_REG);
	Byte2 = BMM150_Read_Reg(Y_H_DATA|READ_REG);
	Byte2 = BMM150_Read_Reg(Y_H_DATA|READ_REG);
	BMM150_XYZ[1]=(Byte2<<8+(Byte1&0xf8))>>3;
	BMM150_XYZ[1]=(Byte1&0xf8)>>3;
	Byte1 = BMM150_Read_Reg(Z_L_DATA|READ_REG);
	Byte1 = BMM150_Read_Reg(Z_L_DATA|READ_REG);
	Byte2 = BMM150_Read_Reg(Z_H_DATA|READ_REG);
	Byte2 = BMM150_Read_Reg(Z_H_DATA|READ_REG);
	BMM150_XYZ[2]=(Byte2<<8+(Byte1&0xfe))>>1;
	BMM150_XYZ[2]=Byte2;
	
	UART_Send_byte(1,0x03);
	UART_Send_byte(1,~0x03);
	UART_Send_byte(1,BMM150_XYZ[0]);
	UART_Send_byte(1,BMM150_XYZ[0]>>8);
	UART_Send_byte(1,BMM150_XYZ[1]);
	UART_Send_byte(1,BMM150_XYZ[1]>>8);
	UART_Send_byte(1,BMM150_XYZ[2]);
	UART_Send_byte(1,BMM150_XYZ[2]>>8);
  UART_Send_byte(1,~0x03);
	UART_Send_byte(1,0x03);
	
}

/***************************************************
函数名称：延时函数
输入参数：ms = 延时的时长
函数功能：延时
***************************************************/
void BMM150_delay_us(uchar num)
{
	unsigned char i;

	while(num>0)
	{
		_nop_();
		i = 6;
		while (--i);
		num--;
	}
}
