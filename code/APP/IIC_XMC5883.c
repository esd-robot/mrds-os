/********************************************
*********************************************************/
#include "IIC_XMC5883.h"
int16 mag_x,mag_y,mag_z;
int16 QMC5883L_Temp;
double MAG_Angle;
void i2cWrite(uint8 reg_, uint8 Data)
{
	ESD_Write_IIC(MAG_ADDRESS, reg_,1,&Data);
}

/**
* @brief	xmc5883初始化,包含传感器校准
* @note
*/ //8G-3000 2G-12000
void xmc5883lInit(void)//默认100HZ,8G量程，连续测量模式
{
#ifdef HMC5883L
	i2cWrite(ConfigRegA, Sample_MAX << 5 | DataOutputRate_Max << 2 | NormalOperation);
	i2cWrite(ConfigRegB, Full_Scale_1_9G << 5);
	i2cWrite(ModeRegister, ContinuousConversion);	
#endif
#ifdef QMC5883L
	i2cWrite(ConfigReg1, Sample_MAX << 6 | Full_Scale_8G << 4 | DataOutputRate_Max << 2 | ContinuousConversion);
	i2cWrite(ConfigReg2, Enable_Interrupt_PIN << 7 | ROL_PNT_Normal << 6 | SOFT_RST_Normal);
	i2cWrite(Period_FBR, 0x01);
#endif
}
void xmc5883lRead(void)
{
#ifdef HMC5883L
	uchar buf[6];
	ESD_Read_IIC(MAG_ADDRESS,MAG_DATA_REGISTER, 6, buf);
	mag_x = buf[0] << 8 | buf[1];
	mag_z = buf[2] << 8 | buf[3];
	mag_y = buf[4] << 8 | buf[5];
#endif
#ifdef QMC5883L
	uchar buf[9];
	ESD_Read_IIC(MAG_ADDRESS,MAG_DATA_REGISTER, 9, buf);
	mag_x = buf[0] | (buf[1] << 8);
	mag_y = buf[2] | (buf[3] << 8);
	mag_z = buf[4] | (buf[5] << 8);
	QMC5883L_Temp = buf[7] | (buf[8] << 8);
#endif
}
void GetMrgAngle(void)    //地磁计角度输出
{
    double mx, my, mz;
    double B[6];
    double tmp3f_x, tmp3f_y, tmp3f_z; //临时变量
    double MagOffset_x, MagOffset_y, MagOffset_z; // 磁力计偏移量参数
    double MagAdjust_x, MagAdjust_y, MagAdjust_z;
    /****************************************************************///antmag软件椭球校准算法
        B[0] = 0.843254905819431; B[1] = -0.109984208524691; B[2] = -0.384186236885065;
        B[3] = 1.34381047987043; B[4] = -0.03595891453399; B[5] = 1.07260521508842;

        MagOffset_x = 0.853893792864808; MagOffset_y = 0.313701074172874; MagOffset_z = 0.473668378107842;
	  xmc5883lRead();
    mx=(float)mag_x/3000;//转换地磁计数据(8G量程)
    my=(float)mag_y/3000;
    mz=(float)mag_z/3000;
	
//	  mx=(float)mag_x/12000;//转换地磁计数据（2G量程）
//    my=(float)mag_y/12000;
//    mz=(float)mag_z/12000;

    tmp3f_x = mx - MagOffset_x;
    tmp3f_y = my - MagOffset_y;
    tmp3f_z = mz - MagOffset_z;
    MagAdjust_x = B[0]*tmp3f_x + B[1]*tmp3f_y +B[2]*tmp3f_z;
    MagAdjust_y = B[1]*tmp3f_x + B[3]*tmp3f_y +B[4]*tmp3f_z;
    MagAdjust_z = B[2]*tmp3f_x + B[4]*tmp3f_y +B[5]*tmp3f_z;
    MAG_Angle = (atan2((double)MagAdjust_y, (double)MagAdjust_x) * 57.3 + 90.0);
    if(MAG_Angle>360.0)
    {
        MAG_Angle -= 360.0;
    }
    if (MAG_Angle <0)
    {
        MAG_Angle += 360.0;
    }
}