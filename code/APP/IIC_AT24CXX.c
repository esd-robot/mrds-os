#include "IIC_AT24CXX.h"
void _24C02_WRITE(uint addr,uchar dat)
{
	ESD_IIC_Start();                  //起始信号
	ESD_IIC_WRITE_ONE_BYTE(0xA0);   //发送设备地址+写信号
	ESD_IIC_WRITE_ONE_BYTE(addr/256);	
	ESD_IIC_WRITE_ONE_BYTE(addr%256);
	ESD_IIC_WRITE_ONE_BYTE(dat); 	//写单个数据 = 发送数据+接收ACK组合
	ESD_IIC_Stop();
}

uchar _24C02_READ(uint addr)
{
	uchar dat;
	
	ESD_IIC_Start();
	ESD_IIC_WRITE_ONE_BYTE(0xA0); 
	ESD_IIC_WRITE_ONE_BYTE(addr/256);	
	ESD_IIC_WRITE_ONE_BYTE(addr%256);
	ESD_IIC_Stop();
	
	ESD_IIC_Start();
	ESD_IIC_WRITE_ONE_BYTE(0xA1); 
	dat=ESD_IIC_READ_NACK_BYTE();
	ESD_IIC_Stop();
	return dat;
}