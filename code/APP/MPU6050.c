#include "MPU6050.h"
int16 mpu6050_gyro_x,mpu6050_gyro_y,mpu6050_gyro_z;
int16 mpu6050_acc_x,mpu6050_acc_y,mpu6050_acc_z;


#define GET_MPU6050_SDA   		 	MPU6050_SDA_PIN
#define MPU6050_SCL_LOW()          	MPU6050_SCL_PIN = 0		//IO口输出低电平
#define MPU6050_SCL_HIGH()         	MPU6050_SCL_PIN = 1		//IO口输出高电平  
#define MPU6050_SDA_LOW()          	MPU6050_SDA_PIN = 0		//IO口输出低电平
#define MPU6050_SDA_HIGH()         	MPU6050_SDA_PIN = 1		//IO口输出高电平

#define ack 1      //主应答
#define no_ack 0   //从应答	

//-------------------------------------------------------------------------------------------------------------------
//  @brief      模拟IIC延时
//  @return     void						
//  @since      v1.0
//  Sample usage:				如果IIC通讯失败可以尝试增加j的值
//-------------------------------------------------------------------------------------------------------------------
static void mpu6050_simiic_delay(void)
{
    uint16 j=MPU6050_IIC_DELAY;   
	while(j--);
}

//内部使用，用户无需调用
static void mpu6050_simiic_start(void)
{
	MPU6050_SDA_HIGH();
	MPU6050_SCL_HIGH();
	mpu6050_simiic_delay();
	MPU6050_SDA_LOW();
	mpu6050_simiic_delay();
	MPU6050_SCL_LOW();
}

//内部使用，用户无需调用
static void mpu6050_simiic_stop(void)
{
	MPU6050_SDA_LOW();
	MPU6050_SCL_LOW();
	mpu6050_simiic_delay();
	MPU6050_SCL_HIGH();
	mpu6050_simiic_delay();
	MPU6050_SDA_HIGH();
	mpu6050_simiic_delay();
}

//主应答(包含ack:SDA=0和no_ack:SDA=0)
//内部使用，用户无需调用
static void mpu6050_simiic_sendack(unsigned char ack_dat)
{
    MPU6050_SCL_LOW();
	mpu6050_simiic_delay();
	if(ack_dat) MPU6050_SDA_LOW();
    else    	MPU6050_SDA_HIGH();

    MPU6050_SCL_HIGH();
    mpu6050_simiic_delay();
    MPU6050_SCL_LOW();
    mpu6050_simiic_delay();
}


static int mpu6050_sccb_waitack(void)
{
    MPU6050_SCL_LOW();

	mpu6050_simiic_delay();
	
	MPU6050_SCL_HIGH();
    mpu6050_simiic_delay();
	
    if(GET_MPU6050_SDA)           //应答为高电平，异常，通信失败
    {

        MPU6050_SCL_LOW();
        return 0;
    }

    MPU6050_SCL_LOW();
	mpu6050_simiic_delay();
    return 1;
}

//字节发送程序
//发送c(可以是数据也可是地址)，送完后接收从应答
//不考虑从应答位
//内部使用，用户无需调用
static void mpu6050_send_ch(uint8 c)
{
	uint8 i = 8;
    while(i--)
    {
        if(c & 0x80)	MPU6050_SDA_HIGH();//SDA 输出数据
        else			MPU6050_SDA_LOW();
        c <<= 1;
        mpu6050_simiic_delay();
        MPU6050_SCL_HIGH();                //SCL 拉高，采集信号
        mpu6050_simiic_delay();
        MPU6050_SCL_LOW();                //SCL 时钟线拉低
    }
	mpu6050_sccb_waitack();
}


//字节接收程序
//接收器件传来的数据，此程序应配合|主应答函数|使用
//内部使用，用户无需调用
static uint8 mpu6050_read_ch(uint8 ack_x)
{
    uint8 i;
    uint8 c;
    c=0;
    MPU6050_SCL_LOW();
    mpu6050_simiic_delay();
    MPU6050_SDA_HIGH();             

    for(i=0;i<8;i++)
    {
        mpu6050_simiic_delay();
        MPU6050_SCL_LOW();         //置时钟线为低，准备接收数据位
        mpu6050_simiic_delay();
        MPU6050_SCL_HIGH();         //置时钟线为高，使数据线上数据有效
        mpu6050_simiic_delay();
        c<<=1;
        if(GET_MPU6050_SDA) 
        {
            c+=1;   //读数据位，将接收的数据存c
        }
    }

	MPU6050_SCL_LOW();
	mpu6050_simiic_delay();
	mpu6050_simiic_sendack(ack_x);
	
    return c;
}


//-------------------------------------------------------------------------------------------------------------------
//  @brief      模拟IIC写数据到设备寄存器函数
//  @param      dev_add			设备地址(低七位地址)
//  @param      reg				寄存器地址
//  @param      dat				写入的数据
//  @return     void						
//  @since      v1.0
//  Sample usage:				
//-------------------------------------------------------------------------------------------------------------------
static void mpu6050_simiic_write_reg(uint8 dev_add, uint8 reg, uint8 dat)
{
	mpu6050_simiic_start();
    mpu6050_send_ch( (dev_add<<1) | 0x00);   //发送器件地址加写位
	mpu6050_send_ch( reg );   				 //发送从机寄存器地址
	mpu6050_send_ch( dat );   				 //发送需要写入的数据
	mpu6050_simiic_stop();
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      模拟IIC从设备寄存器读取数据
//  @param      dev_add			设备地址(低七位地址)
//  @param      reg				寄存器地址
//  @param      type			选择通信方式是IIC  还是 SCCB
//  @return     uint8			返回寄存器的数据			
//  @since      v1.0
//  Sample usage:				
//-------------------------------------------------------------------------------------------------------------------
uint8 mpu6050_simiic_read_reg(uint8 dev_add, uint8 reg)
{
	uint8 dat;
	mpu6050_simiic_start();
    mpu6050_send_ch( (dev_add<<1) | 0x00);  //发送器件地址加写位
	mpu6050_send_ch( reg );   				//发送从机寄存器地址

	
	mpu6050_simiic_start();
	mpu6050_send_ch( (dev_add<<1) | 0x01);  //发送器件地址加读位
	dat = mpu6050_read_ch(no_ack);   				//读取数据
	mpu6050_simiic_stop();
	
	return dat;
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      模拟IIC读取多字节数据
//  @param      dev_add			设备地址(低七位地址)
//  @param      reg				寄存器地址
//  @param      dat_add			数据保存的地址指针
//  @param      num				读取字节数量
//  @param      type			选择通信方式是IIC  还是 SCCB
//  @return     uint8			返回寄存器的数据			
//  @since      v1.0
//  Sample usage:				
//-------------------------------------------------------------------------------------------------------------------
void mpu6050_simiic_read_regs(uint8 dev_add, uint8 reg, uint8 *dat_add, uint8 num)
{
	mpu6050_simiic_start();
    mpu6050_send_ch( (dev_add<<1) | 0x00);  //发送器件地址加写位
	mpu6050_send_ch( reg );   				//发送从机寄存器地址

	
	mpu6050_simiic_start();
	mpu6050_send_ch( (dev_add<<1) | 0x01);  //发送器件地址加读位
    while(--num)
    {
        *dat_add = mpu6050_read_ch(ack); //读取数据
        dat_add++;
    }
    *dat_add = mpu6050_read_ch(no_ack); //读取数据
	mpu6050_simiic_stop();
}
//-------------------------------------------------------------------------------------------------------------------
//  @brief      初始化MPU6050
//  @param      NULL
//  @return     void					
//  @since      v1.0
//  Sample usage:				
//-------------------------------------------------------------------------------------------------------------------
uint8 mpu6050_init(void)//使用模拟IIC初始化MPU6050
{
    Delay_X_mS(10);                                   //上电延时

    mpu6050_simiic_write_reg(MPU6050_DEV_ADDR, PWR_MGMT_1, 0x00);	//解除休眠状态
    mpu6050_simiic_write_reg(MPU6050_DEV_ADDR, SMPLRT_DIV, 0x07);   //125HZ采样率
    mpu6050_simiic_write_reg(MPU6050_DEV_ADDR, MPU6050_CONFIG, 0x04);       //
    mpu6050_simiic_write_reg(MPU6050_DEV_ADDR, GYRO_CONFIG, 0x08);  //1000  //00 - 250  ； 08 -500 ；10 -1000  ；18 -2000
    mpu6050_simiic_write_reg(MPU6050_DEV_ADDR, ACCEL_CONFIG, 0x08); //8g    // 00 -2G   ;   08 -4G ;   10 -8G ;   18 -16G
	mpu6050_simiic_write_reg(MPU6050_DEV_ADDR, User_Control, 0x00);
    mpu6050_simiic_write_reg(MPU6050_DEV_ADDR, INT_PIN_CFG, 0x02);
	return 0;
}

void mpu6050_iic_init(void)  //使用硬件IIC初始化MPU6050
{
	Delay_X_mS(10); 
	ESD_Write_IIC(MPU6050_DEV_ADDR, PWR_MGMT_1,1, 0x00);	//解除休眠状态
	ESD_Write_IIC(MPU6050_DEV_ADDR, SMPLRT_DIV,1, 0x07);   //125HZ采样率
	ESD_Write_IIC(MPU6050_DEV_ADDR, MPU6050_CONFIG,1, 0x04);  //21HZ滤波 延时A8.5ms G8.3ms  此处取值应相当注意，延时与系统周期相近为宜
	ESD_Write_IIC(MPU6050_DEV_ADDR, GYRO_CONFIG,1, 0x10);  //±1000°/s
	ESD_Write_IIC(MPU6050_DEV_ADDR, ACCEL_CONFIG,1, 0x10); //±8g
	ESD_Write_IIC(MPU6050_DEV_ADDR, User_Control,1, 0x00);
	ESD_Write_IIC(MPU6050_DEV_ADDR, INT_PIN_CFG,1, 0x02);
}


//-------------------------------------------------------------------------------------------------------------------
//  @brief      获取MPU6050加速度计数据
//  @param      NULL
//  @return     void
//  @since      v1.0
//  Sample usage:				执行该函数后，直接查看对应的变量即可
//-------------------------------------------------------------------------------------------------------------------
void mpu6050_get_accdata(void)
{
    uint8 dat[6];

    mpu6050_simiic_read_regs(MPU6050_DEV_ADDR, ACCEL_XOUT_H, dat, 6);  
    mpu6050_acc_x = (int16)(((uint16)dat[0]<<8 | dat[1]));
    mpu6050_acc_y = (int16)(((uint16)dat[2]<<8 | dat[3]));
    mpu6050_acc_z = (int16)(((uint16)dat[4]<<8 | dat[5]));
}
//-------------------------------------------------------------------------------------------------------------------
//  @brief      获取MPU6050陀螺仪数据
//  @param      NULL
//  @return     void
//  @since      v1.0
//  Sample usage:				执行该函数后，直接查看对应的变量即可
//-------------------------------------------------------------------------------------------------------------------
void mpu6050_get_gyro(void)
{
    uint8 dat[6];

    mpu6050_simiic_read_regs(MPU6050_DEV_ADDR, GYRO_XOUT_H, dat, 6);  
    mpu6050_gyro_x = (int16)(((uint16)dat[0]<<8 | dat[1]));
    mpu6050_gyro_y = (int16)(((uint16)dat[2]<<8 | dat[3]));
    mpu6050_gyro_z = (int16)(((uint16)dat[4]<<8 | dat[5]));
}

//*********************************************************************
//****************四元数角度计算****************************************
//*********************************************************************
#define	pi		3.14159265f                           
#define	Kp		0.8f                        
#define	Ki		0.001f                         
#define	halfT	0.008f           

float idata q0=1,q1=0,q2=0,q3=0;   
float idata exInt=0,eyInt=0,ezInt=0;  
float	data  AngleX=0,AngleY=0,AngleX_old=0;					//四元数解算出的欧拉角

void IMUupdate(float gx, float gy, float gz, float ax, float ay, float az)
{
	float data norm;
	float idata vx, vy, vz;
	float idata ex, ey, ez;

	norm = sqrt(ax*ax + ay*ay + az*az);	//把加速度计的三维向量转成单维向量   
	ax = ax / norm;
	ay = ay / norm;
	az = az / norm;

		//	下面是把四元数换算成《方向余弦矩阵》中的第三列的三个元素。 
		//	根据余弦矩阵和欧拉角的定义，地理坐标系的重力向量，转到机体坐标系，正好是这三个元素
		//	所以这里的vx vy vz，其实就是当前的欧拉角（即四元数）的机体坐标参照系上，换算出来的
		//	重力单位向量。
	vx = 2*(q1*q3 - q0*q2);
	vy = 2*(q0*q1 + q2*q3);
	vz = q0*q0 - q1*q1 - q2*q2 + q3*q3 ;

	ex = (ay*vz - az*vy) ;
	ey = (az*vx - ax*vz) ;
	ez = (ax*vy - ay*vx) ;

	exInt = exInt + ex * Ki;
	eyInt = eyInt + ey * Ki;
	ezInt = ezInt + ez * Ki;

	gx = gx + Kp*ex + exInt;
	gy = gy + Kp*ey + eyInt;
	gz = gz + Kp*ez + ezInt;

	q0 = q0 + (-q1*gx - q2*gy - q3*gz) * halfT;
	q1 = q1 + ( q0*gx + q2*gz - q3*gy) * halfT;
	q2 = q2 + ( q0*gy - q1*gz + q3*gx) * halfT;
	q3 = q3 + ( q0*gz + q1*gy - q2*gx) * halfT;

	norm = sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3);
	q0 = q0 / norm;
	q1 = q1 / norm;
	q2 = q2 / norm;
	q3 = q3 / norm;

	AngleX = asin(2*(q0*q2 - q1*q3 )) * 57.2957795f; // 俯仰   换算成度
	AngleY = asin(2*(q0*q1 + q2*q3 )) * 57.2957795f; // 横滚
}

/**************************************************************************
              西安建筑科技大学华清学院-工程训练中心
**************************************************************************/
float K1 =0.02; 
float angle, angle_dot; 	
float Q_angle=0.001;// 过程噪声的协方差
float Q_gyro=0.003;//0.003 过程噪声的协方差 过程噪声的协方差为一个一行两列矩阵
float R_angle=0.001;// 测量噪声的协方差 既测量偏差
float d_t=0.008;//                 
char  C_0 = 1;
float Q_bias, Angle_err;
float PCt_0, PCt_1, E;
float K_0, K_1, t_0, t_1;
float Pdot[4] ={0,0,0,0};
float PP[2][2] = { { 1, 0 },{ 0, 1 } };

/**************************************************************************
函数功能：简易卡尔曼滤波
入口参数：加速度、角速度
返回  值：无
**************************************************************************/
void Kalman_Filter(float Accel,float Gyro)		
{
	angle+=(Gyro - Q_bias) * d_t; //先验估计
	Pdot[0]=Q_angle - PP[0][1] - PP[1][0]; // Pk-先验估计误差协方差的微分

	Pdot[1]=-PP[1][1];
	Pdot[2]=-PP[1][1];
	Pdot[3]=Q_gyro;
	PP[0][0] += Pdot[0] * d_t;   // Pk-先验估计误差协方差微分的积分
	PP[0][1] += Pdot[1] * d_t;   // =先验估计误差协方差
	PP[1][0] += Pdot[2] * d_t;
	PP[1][1] += Pdot[3] * d_t;
		
	Angle_err = Accel - angle;	//zk-先验估计
	
	PCt_0 = C_0 * PP[0][0];
	PCt_1 = C_0 * PP[1][0];
	
	E = R_angle + C_0 * PCt_0;
	
	K_0 = PCt_0 / E;
	K_1 = PCt_1 / E;
	
	t_0 = PCt_0;
	t_1 = C_0 * PP[0][1];

	PP[0][0] -= K_0 * t_0;		 //后验估计误差协方差
	PP[0][1] -= K_0 * t_1;
	PP[1][0] -= K_1 * t_0;
	PP[1][1] -= K_1 * t_1;
		
	angle	+= K_0 * Angle_err;	 //后验估计
	Q_bias	+= K_1 * Angle_err;	 //后验估计
	angle_dot   = Gyro - Q_bias;	 //输出值(后验估计)的微分=角速度
}

/**************************************************************************
函数功能：一阶互补滤波
入口参数：加速度、角速度
返回  值：无
**************************************************************************/
void Yijielvbo(float angle_m, float gyro_m)
{
   angle = K1 * angle_m+ (1-K1) * (angle + gyro_m * 0.005);
}

