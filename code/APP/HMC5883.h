
#include "config.h"
	
#define uint unsigned int   
#define u8 unsigned char 
//typedef unsigned char BYTE;
//typedef unsigned short WORD;

									
extern void Init_HMC5883(void);            //初始化5883
extern void  Single_Write_HMC5883(uchar REG_Address,uchar REG_data);   //单个写入数据
extern uchar Single_Read_HMC5883(uchar REG_Address);                   //单个读取内部寄存器数据
extern void  Multiple_Read_HMC5883();                                  //连续的读取内部寄存器数据
//以下是模拟iic使用函数-------------
void HMC5883_Delay5us();
void HMC5883_Delay5ms();
void HMC5883_Start();
void HMC5883_Stop();
void HMC5883_SendACK(bit ack);
bit  HMC5883_RecvACK();
void HMC5883_SendByte(u8 dat);
u8 HMC5883_RecvByte();
void HMC5883_ReadPage();
void HMC5883_WritePage();

extern u8 HMC_BUF[8];

