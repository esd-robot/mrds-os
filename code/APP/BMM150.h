
#include "config.h"
	
#define uint unsigned int   
#define uchar unsigned char 

extern uint BMM150_XYZ[3];

extern uchar BMM150_SPI_RW(uchar BMMbyte0);
extern uchar BMM150_Write_Reg(uchar reg,uchar value);
extern uchar BMM150_Read_Reg(uchar reg);
extern void BMM150_Init(void);
extern void BMM150_Get_Data(void);
extern void BMM150_delay_us(uchar num);

