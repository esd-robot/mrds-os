/********************************************
*********************************************************/
#include "IIC_DS3231.h"
#include "config.h"

/**********  寄存器地址   *************/

/*------------------------------------------------------------------*/

#define DS3231_WriteAddress 0xD0    //器件写地址
#define DS3231_ReadAddress  0xD1    //器件读地址
#define DS3231_SECOND       0x00    //秒
#define DS3231_MINUTE       0x01    //分
#define DS3231_HOUR         0x02    //时
#define DS3231_WEEK         0x03    //星期
#define DS3231_DAY          0x04    //日
#define DS3231_MONTH        0x05    //月
#define DS3231_YEAR         0x06    //年
//闹钟一          
#define DS3231_SALARM1ECOND 0x07    //秒
#define DS3231_ALARM1MINUTE 0x08    //时
#define DS3231_ALARM1HOUR   0x09    //分
#define DS3231_ALARM1WEEK   0x0A    //星期/日
//闹钟二 
#define DS3231_ALARM2MINUTE 0x0b    //分
#define DS3231_ALARM2HOUR   0x0c    //时
#define DS3231_ALARM2WEEK   0x0d    //星期/日
#define DS3231_CONTROL      0x0e    //控制寄存器
#define DS3231_STATUS       0x0f    //状态寄存器
#define BSY                 2       //忙
#define OSF                 7       //振荡器停止标志
#define DS3231_XTAL         0x10    //晶体老化寄存器
#define DS3231_TEMPERATUREH 0x11    //温度寄存器高(8位) 
#define DS3231_TEMPERATUREL 0x12    //温度寄存器低(低2位) 
//--声明一些要使用的常量--//

																	
uchar DS_Year,DS_Moon,DS_Day,DS_Week,DS_Hour,DS_Min,DS_Sec,DS_Temp;  //定义时间
uchar xdata DS3231_dat[7];


void write_byte(uchar addr, uchar write_data) 
{      
	ESD_IIC_Start();      
	ESD_IIC_WRITE_ONE_BYTE(DS3231_ADDR); 
  ESD_IIC_WRITE_ONE_BYTE(addr);
	ESD_IIC_WRITE_ONE_BYTE(write_data);		
	ESD_IIC_Stop();
}

uchar read_random(uchar random_addr)
{
	uchar read_data;
	
	ESD_IIC_Start();
	ESD_IIC_WRITE_ONE_BYTE(DS3231_ADDR);
	ESD_IIC_WRITE_ONE_BYTE(random_addr);
	ESD_IIC_Start();
	ESD_IIC_WRITE_ONE_BYTE(DS3231_ADDR+1);
	read_data = ESD_IIC_READ_NACK_BYTE();
	ESD_IIC_Stop();	
	
	return read_data; 
}



//初始化DS3231时间，根据需要请参考pdf进行修改****
void Init_DS3231(uchar year,uchar moon,uchar day,uchar week,uchar hour,uchar min,uchar sec)
{
	uchar dat[7]={0};  
	
	dat[0]=HEX2BCD(sec);
	dat[1]=HEX2BCD(min);
	dat[2]=HEX2BCD(hour);
	dat[3]=HEX2BCD(week);
	dat[4]=HEX2BCD(day);
	dat[5]=HEX2BCD(moon);
	dat[6]=HEX2BCD(year);
	

	ESD_Write_IIC(DS3231_ADDR>>1,0x00,7,dat);

	
//	temp=HEX2BCD(year); 
//	write_byte(DS3231_YEAR,temp);    //年
//	temp=HEX2BCD(moon);      
//	write_byte(DS3231_MONTH,temp);   //月    
//	temp=HEX2BCD(day);      
//	write_byte(DS3231_DAY,temp);     //日     
//	temp=HEX2BCD(hour);      
//	write_byte(DS3231_HOUR,temp);    //时      
//	temp=HEX2BCD(min);      
//	write_byte(DS3231_MINUTE,temp);  //分     
//	temp=HEX2BCD(sec);      
//	write_byte(DS3231_SECOND,temp);  //秒
}





void IIC_Read_DS3231(void)  //大于50ms重复调用此函数，获取时间信息
{
//	uchar Htemp1,Mtemp1,Stemp1,Ytemp1,Motemp1,Dtemp1,Ttemp1,Ttemp2,Ttemp3,Ttemp4;    
	
	ESD_Read_IIC(DS3231_ADDR>>1,0x00,7,DS3231_dat);
	
	DS_Sec=BCD2HEX(DS3231_dat[0]); 			  //秒
	DS_Min=BCD2HEX(DS3231_dat[1]);  				//分       
	DS_Hour=BCD2HEX(DS3231_dat[2]&0x3f);		//时 
	DS_Week=BCD2HEX(DS3231_dat[3]);				//星期 
	DS_Day=BCD2HEX(DS3231_dat[4]); 				//日
	DS_Moon=BCD2HEX(DS3231_dat[5]); 	//月
	DS_Year=BCD2HEX(DS3231_dat[6]); 				//年 
	
//	
//	Htemp1=read_random(DS3231_HOUR);    //时,24小时制   
//	Htemp1&=0x3f;                        
//	Hour=BCD2HEX(Htemp1);          
//	Mtemp1=read_random(DS3231_MINUTE);  //分
//  Min=BCD2HEX(Mtemp1);          
//	Stemp1=read_random(DS3231_SECOND);  //秒 
//	Sec=BCD2HEX(Stemp1);  

//	Ytemp1=read_random(DS3231_YEAR);        //年  
//	Year=BCD2HEX(Ytemp1);	
//	Motemp1=read_random(DS3231_MONTH);       //月   
//	Moon=BCD2HEX(Motemp1);
//	Dtemp1=read_random(DS3231_DAY);         //日  
//	Day=BCD2HEX(Dtemp1);
//	
//	Ttemp1=read_random(DS3231_TEMPERATUREH);    //温度高字节   
//	Ttemp2=BCD2HEX(Ttemp1);          
//	Ttemp3=read_random(DS3231_TEMPERATUREL);    //温度低字节    
//	Ttemp4=Ttemp3&0xc0;
//	
//	Temp_H=Ttemp2;
//	Temp_L=(Ttemp4>>6)*25;
//	//Temp=Ttemp2*256+Ttemp4;
//	Temp=Ttemp2;
}
