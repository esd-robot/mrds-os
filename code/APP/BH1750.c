/********************************************
*********************************************************/
#include "BH1750.h"
#include "config.h"

typedef unsigned char BYTE;
typedef unsigned short WORD;

sbit GY30_1_SCL=P0^0;      //IIC时钟引脚定义
sbit GY30_1_SDA=P0^1;      //IIC数据引脚定义

sbit	    GY30_2_SCL=P0^2;      //IIC时钟引脚定义
sbit  	  GY30_2_SDA=P0^3;      //IIC数据引脚定义

sbit	    GY30_3_SCL=P0^4;      //IIC时钟引脚定义
sbit  	  GY30_3_SDA=P0^5;      //IIC数据引脚定义

sbit	    GY30_4_SCL=P0^6;      //IIC时钟引脚定义
sbit  	  GY30_4_SDA=P0^7;      //IIC数据引脚定义

sbit	    GY30_5_SCL=P2^0;      //IIC时钟引脚定义
sbit  	  GY30_5_SDA=P2^1;      //IIC数据引脚定义

sbit	    GY30_6_SCL=P2^2;      //IIC时钟引脚定义
sbit  	  GY30_6_SDA=P2^3;      //IIC数据引脚定义

sbit	    GY30_7_SCL=P2^4;      //IIC时钟引脚定义
sbit  	  GY30_7_SDA=P2^5;      //IIC数据引脚定义

sbit	    GY30_8_SCL=P2^6;      //IIC时钟引脚定义
sbit  	  GY30_8_SDA=P2^7;      //IIC数据引脚定义


/**********  寄存器地址   *************/
#define	  SlaveAddress   0x46 //定义器件在IIC总线中的从地址,根据ALT  ADDRESS地址引脚不同修改
                              //ALT  ADDRESS引脚接地时地址为0x46，接电源时地址为0xB8
															


unsigned char BH1750_BUF[8][8];                         //8路IIC接收数据缓存区




/*------------------------------------------------------------------*/

/**************************************
延时5微秒(STC8A-24MHZ)
不同的工作环境,需要调整此函数
当改用1T的MCU时,请调整此延时函数
**************************************/
void Delay5us(void)
{
	unsigned char i;

	i = 38;
	while (--i);
}

/**************************************
延时5毫秒(STC8A-24MHZ)
不同的工作环境,需要调整此函数
当改用1T的MCU时,请调整此延时函数
**************************************/
void Delay5ms()
{
  unsigned char i, j;

	_nop_();
	_nop_();
	i = 156;
	j = 213;
	do
	{
		while (--j);
	} while (--i);
}

/**************************************
起始信号
**************************************/
void BH1750_Start(char Pass)
{
	switch(Pass)
	{
		case 0:
		{
			GY30_1_SDA = 1;                    //拉高数据线
			GY30_1_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_1_SDA = 0;                    //产生下降沿
			Delay5us();                 //延时
			GY30_1_SCL = 0;                    //拉低时钟线
			break;
		}
		case 1:
		{
			GY30_2_SDA = 1;                    //拉高数据线
			GY30_2_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_2_SDA = 0;                    //产生下降沿
			Delay5us();                 //延时
			GY30_2_SCL = 0;                    //拉低时钟线
			break;
		}
		case 2:
		{
			GY30_3_SDA = 1;                    //拉高数据线
			GY30_3_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_3_SDA = 0;                    //产生下降沿
			Delay5us();                 //延时
			GY30_3_SCL = 0;                    //拉低时钟线
			break;
		}
		case 3:
		{
			GY30_4_SDA = 1;                    //拉高数据线
			GY30_4_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_4_SDA = 0;                    //产生下降沿
			Delay5us();                 //延时
			GY30_4_SCL = 0;                    //拉低时钟线
			break;
		}
		case 4:
		{
			GY30_5_SDA = 1;                    //拉高数据线
			GY30_5_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_5_SDA = 0;                    //产生下降沿
			Delay5us();                 //延时
			GY30_5_SCL = 0;                    //拉低时钟线
			break;
		}
		case 5:
		{
			GY30_6_SDA = 1;                    //拉高数据线
			GY30_6_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_6_SDA = 0;                    //产生下降沿
			Delay5us();                 //延时
			GY30_6_SCL = 0;                    //拉低时钟线
			break;
		}
		case 6:
		{
			GY30_7_SDA = 1;                    //拉高数据线
			GY30_7_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_7_SDA = 0;                    //产生下降沿
			Delay5us();                 //延时
			GY30_7_SCL = 0;                    //拉低时钟线
			break;
		}
		case 7:
		{
			GY30_8_SDA = 1;                    //拉高数据线
			GY30_8_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_8_SDA = 0;                    //产生下降沿
			Delay5us();                 //延时
			GY30_8_SCL = 0;                    //拉低时钟线
			break;
		}
	}
    
}

/**************************************
停止信号
**************************************/
void BH1750_Stop(char Pass)
{
	switch(Pass)
	{
		case 0:
		{
			GY30_1_SDA = 0;                    //拉低数据线
			GY30_1_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_1_SDA = 1;                    //产生上升沿
			Delay5us();                 //延时
			break;
		}
		case 1:
		{
			GY30_2_SDA = 0;                    //拉低数据线
			GY30_2_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_2_SDA = 1;                    //产生上升沿
			Delay5us();                 //延时
			break;
		}
		case 2:
		{
			GY30_3_SDA = 0;                    //拉低数据线
			GY30_3_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_3_SDA = 1;                    //产生上升沿
			Delay5us();                 //延时
			break;
		}
		case 3:
		{
			GY30_4_SDA = 0;                    //拉低数据线
			GY30_4_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_4_SDA = 1;                    //产生上升沿
			Delay5us();                 //延时
			break;
		}
		case 4:
		{
			GY30_5_SDA = 0;                    //拉低数据线
			GY30_5_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_5_SDA = 1;                    //产生上升沿
			Delay5us();                 //延时
			break;
		}
		case 5:
		{
			GY30_6_SDA = 0;                    //拉低数据线
			GY30_6_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_6_SDA = 1;                    //产生上升沿
			Delay5us();                 //延时
			break;
		}
		case 6:
		{
			GY30_7_SDA = 0;                    //拉低数据线
			GY30_7_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_7_SDA = 1;                    //产生上升沿
			Delay5us();                 //延时
			break;
		}
		case 7:
		{
			GY30_8_SDA = 0;                    //拉低数据线
			GY30_8_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_8_SDA = 1;                    //产生上升沿
			Delay5us();                 //延时
			break;
		}
	}
    
}

/**************************************
发送应答信号
入口参数:ack (0:ACK 1:NAK)
**************************************/
void BH1750_SendACK(char Pass,bit ack)
{
	switch(Pass)
	{
		case 0:
		{
			GY30_1_SDA = ack;                  //写应答信号
			GY30_1_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_1_SCL = 0;                    //拉低时钟线
			Delay5us();                 //延时
			break;
		}
		case 1:
		{
			GY30_2_SDA = ack;                  //写应答信号
			GY30_2_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_2_SCL = 0;                    //拉低时钟线
			Delay5us();                 //延时
			break;
		}
		case 2:
		{
			GY30_3_SDA = ack;                  //写应答信号
			GY30_3_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_3_SCL = 0;                    //拉低时钟线
			Delay5us();                 //延时
			break;
		}
		case 3:
		{
			GY30_4_SDA = ack;                  //写应答信号
			GY30_4_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_4_SCL = 0;                    //拉低时钟线
			Delay5us();                 //延时
			break;
		}
		case 4:
		{
			GY30_5_SDA = ack;                  //写应答信号
			GY30_5_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_5_SCL = 0;                    //拉低时钟线
			Delay5us();                 //延时
			break;
		}
		case 5:
		{
			GY30_6_SDA = ack;                  //写应答信号
			GY30_6_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_6_SCL = 0;                    //拉低时钟线
			Delay5us();                 //延时
			break;
		}
		case 6:
		{
			GY30_7_SDA = ack;                  //写应答信号
			GY30_7_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_7_SCL = 0;                    //拉低时钟线
			Delay5us();                 //延时
			break;
		}
		case 7:
		{
			GY30_8_SDA = ack;                  //写应答信号
			GY30_8_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			GY30_8_SCL = 0;                    //拉低时钟线
			Delay5us();                 //延时
			break;
		}
	}
    
}

/**************************************
接收应答信号
**************************************/
bit BH1750_RecvACK(char Pass)
{
	switch(Pass)
	{
		case 0:
		{
			GY30_1_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			CY = GY30_1_SDA;                   //读应答信号
			GY30_1_SCL = 0;                    //拉低时钟线
			Delay5us();                 //延时
			break;
		}
		case 1:
		{
			GY30_2_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			CY = GY30_2_SDA;                   //读应答信号
			GY30_2_SCL = 0;                    //拉低时钟线
			Delay5us();                 //延时
			break;
		}
		case 2:
		{
			GY30_3_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			CY = GY30_3_SDA;                   //读应答信号
			GY30_3_SCL = 0;                    //拉低时钟线
			Delay5us();                 //延时
			break;
		}
		case 3:
		{
			GY30_4_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			CY = GY30_4_SDA;                   //读应答信号
			GY30_4_SCL = 0;                    //拉低时钟线
			Delay5us();                 //延时
			break;
		}
		case 4:
		{
			GY30_5_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			CY = GY30_5_SDA;                   //读应答信号
			GY30_5_SCL = 0;                    //拉低时钟线
			Delay5us();                 //延时
			break;
		}
		case 5:
		{
			GY30_6_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			CY = GY30_6_SDA;                   //读应答信号
			GY30_6_SCL = 0;                    //拉低时钟线
			Delay5us();                 //延时
			break;
		}
		case 6:
		{
			GY30_7_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			CY = GY30_7_SDA;                   //读应答信号
			GY30_7_SCL = 0;                    //拉低时钟线
			Delay5us();                 //延时
			break;
		}
		case 7:
		{
			GY30_8_SCL = 1;                    //拉高时钟线
			Delay5us();                 //延时
			CY = GY30_8_SDA;                   //读应答信号
			GY30_8_SCL = 0;                    //拉低时钟线
			Delay5us();                 //延时
			break;
		}
	}
    

    return CY;
}

/**************************************
向IIC总线发送一个字节数据
**************************************/
void BH1750_SendByte(char Pass,unsigned char dat)
{
    BYTE i;

    for (i=0; i<8; i++)         //8位计数器
    {
			switch(Pass)
			{
				case 0:
				{
					dat <<= 1;              //移出数据的最高位
					GY30_1_SDA = CY;               //送数据口
					GY30_1_SCL = 1;                //拉高时钟线
					Delay5us();             //延时
					GY30_1_SCL = 0;                //拉低时钟线
					Delay5us();             //延时
					break;
				}
				case 1:
				{
					dat <<= 1;              //移出数据的最高位
					GY30_2_SDA = CY;               //送数据口
					GY30_2_SCL = 1;                //拉高时钟线
					Delay5us();             //延时
					GY30_2_SCL = 0;                //拉低时钟线
					Delay5us();             //延时
					break;
				}
				case 2:
				{
					dat <<= 1;              //移出数据的最高位
					GY30_3_SDA = CY;               //送数据口
					GY30_3_SCL = 1;                //拉高时钟线
					Delay5us();             //延时
					GY30_3_SCL = 0;                //拉低时钟线
					Delay5us();             //延时
					break;
				}
				case 3:
				{
					dat <<= 1;              //移出数据的最高位
					GY30_4_SDA = CY;               //送数据口
					GY30_4_SCL = 1;                //拉高时钟线
					Delay5us();             //延时
					GY30_4_SCL = 0;                //拉低时钟线
					Delay5us();             //延时
					break;
				}
				case 4:
				{
					dat <<= 1;              //移出数据的最高位
					GY30_5_SDA = CY;               //送数据口
					GY30_5_SCL = 1;                //拉高时钟线
					Delay5us();             //延时
					GY30_5_SCL = 0;                //拉低时钟线
					Delay5us();             //延时
					break;
				}
				case 5:
				{
					dat <<= 1;              //移出数据的最高位
					GY30_6_SDA = CY;               //送数据口
					GY30_6_SCL = 1;                //拉高时钟线
					Delay5us();             //延时
					GY30_6_SCL = 0;                //拉低时钟线
					Delay5us();             //延时
					break;
				}
				case 6:
				{
					dat <<= 1;              //移出数据的最高位
					GY30_7_SDA = CY;               //送数据口
					GY30_7_SCL = 1;                //拉高时钟线
					Delay5us();             //延时
					GY30_7_SCL = 0;                //拉低时钟线
					Delay5us();             //延时
					break;
				}
				case 7:
				{
					dat <<= 1;              //移出数据的最高位
					GY30_8_SDA = CY;               //送数据口
					GY30_8_SCL = 1;                //拉高时钟线
					Delay5us();             //延时
					GY30_8_SCL = 0;                //拉低时钟线
					Delay5us();             //延时
					break;
				}
			}
        
    }
    BH1750_RecvACK(Pass);
}

/**************************************
从IIC总线接收一个字节数据
**************************************/
BYTE BH1750_RecvByte(char Pass)
{
    BYTE i;
    BYTE dat = 0;

	switch(Pass)
	{
		case 0 :GY30_1_SDA = 1; break;                   //使能内部上拉,准备读取数据,
		case 1 :GY30_2_SDA = 1; break;                   //使能内部上拉,准备读取数据
		case 2 :GY30_3_SDA = 1; break;                   //使能内部上拉,准备读取数据
		case 3 :GY30_4_SDA = 1; break;                   //使能内部上拉,准备读取数据
		case 4 :GY30_5_SDA = 1; break;                   //使能内部上拉,准备读取数据
		case 5 :GY30_6_SDA = 1; break;                   //使能内部上拉,准备读取数据
		case 6 :GY30_7_SDA = 1; break;                   //使能内部上拉,准备读取数据
		case 7 :GY30_8_SDA = 1; break;                   //使能内部上拉,准备读取数据
	}
    
    for (i=0; i<8; i++)         //8位计数器
    {
			switch(Pass)
			{
				case 0: 
				{
					dat <<= 1;
					GY30_1_SCL = 1;                //拉高时钟线
					Delay5us();             //延时
					dat |= GY30_1_SDA;             //读数据               
					GY30_1_SCL = 0;                //拉低时钟线
					Delay5us();             //延时
					break;
				}
				case 1: 
				{
					dat <<= 1;
					GY30_2_SCL = 1;                //拉高时钟线
					Delay5us();             //延时
					dat |= GY30_2_SDA;             //读数据               
					GY30_2_SCL = 0;                //拉低时钟线
					Delay5us();             //延时
					break;
				}
				case 2: 
				{
					dat <<= 1;
					GY30_3_SCL = 1;                //拉高时钟线
					Delay5us();             //延时
					dat |= GY30_3_SDA;             //读数据               
					GY30_3_SCL = 0;                //拉低时钟线
					Delay5us();             //延时
					break;
				}
				case 3: 
				{
					dat <<= 1;
					GY30_4_SCL = 1;                //拉高时钟线
					Delay5us();             //延时
					dat |= GY30_4_SDA;             //读数据               
					GY30_4_SCL = 0;                //拉低时钟线
					Delay5us();             //延时
					break;
				}
				case 4: 
				{
					dat <<= 1;
					GY30_5_SCL = 1;                //拉高时钟线
					Delay5us();             //延时
					dat |= GY30_5_SDA;             //读数据               
					GY30_5_SCL = 0;                //拉低时钟线
					Delay5us();             //延时
					break;
				}
				case 5: 
				{
					dat <<= 1;
					GY30_6_SCL = 1;                //拉高时钟线
					Delay5us();             //延时
					dat |= GY30_6_SDA;             //读数据               
					GY30_6_SCL = 0;                //拉低时钟线
					Delay5us();             //延时
					break;
				}
				case 6: 
				{
					dat <<= 1;
					GY30_7_SCL = 1;                //拉高时钟线
					Delay5us();             //延时
					dat |= GY30_7_SDA;             //读数据               
					GY30_7_SCL = 0;                //拉低时钟线
					Delay5us();             //延时
					break;
				}
				case 7: 
				{
					dat <<= 1;
					GY30_8_SCL = 1;                //拉高时钟线
					Delay5us();             //延时
					dat |= GY30_8_SDA;             //读数据               
					GY30_8_SCL = 0;                //拉低时钟线
					Delay5us();             //延时
					break;
				}
			}
        
    }
    return dat;
}
//**************************************
//给I2C设备写一个字节数据
//**************************************
void Single_Write_BH1750(char Pass,uchar REG_Address)
{
    BH1750_Start(Pass);                  //起始信号
    BH1750_SendByte(Pass,SlaveAddress);   //发送设备地址+写信号
    BH1750_SendByte(Pass,REG_Address);    //内部寄存器地址，
  //  BH1750_SendByte(REG_data);       //内部寄存器数据，
    BH1750_Stop(Pass);                   //发送停止信号
}

//*********************************************************
//
//连续读出BH1750内部数据
//
//*********************************************************
void Multiple_read_BH1750(char Pass)
{   uchar i;	
    BH1750_Start(Pass);                          //起始信号
    BH1750_SendByte(Pass,SlaveAddress+1);         //发送设备地址+读信号
	
	 for (i=0; i<3; i++)                      //连续读取2个地址数据，存储中BUF
    {
        BH1750_BUF[Pass][i] = BH1750_RecvByte(Pass);          //BUF[0]存储0x32地址中的数据
        if (i == 3)
        {

           BH1750_SendACK(Pass,1);                //最后一个数据需要回NOACK
        }
        else
        {		
          BH1750_SendACK(Pass,0);                //回应ACK
       }
   }

    BH1750_Stop(Pass);                          //停止信号
    Delay5ms();
}


//初始化BH1750，根据需要请参考pdf进行修改****
void Init_BH1750(char n)
{
	switch(n)
	{
	case 0: P0PU|=0x03;break;
	case 1: P0PU|=0x0c;break;
	case 2: P0PU|=0x30;break;
	case 3: P0PU|=0xc0;break;
	case 4: P2PU|=0x03;break;
	case 5: P2PU|=0x0c;break;
	case 6: P2PU|=0x30;break;
	case 7: P2PU|=0xc0;break;
	}

	Single_Write_BH1750(n,0x01);  
}
