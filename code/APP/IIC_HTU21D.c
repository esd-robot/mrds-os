/********************************************
*********************************************************/
#include "IIC_HTU21D.h"
#include "config.h"

/**********  寄存器地址   *************/

/*------------------------------------------------------------------*/

#define HTU21D_TEMP 0xF3   //触发温度测量
#define HTU21D_HUMI 0xF5	 //触发湿度测量
#define HTU21D_RESET 0xFE	 //触发软复位


float HTU21D_Temp=-99,HTU21D_Humi=-99;  //温湿度缓存
bit HTU21D_Mode=0;


//初始化HMC5883，根据需要请参考pdf进行修改****
void Init_HTU21D()
{
	ESD_Init_IIC(IIC_2); //IIC初始化,IIC通道2
	
	ESD_IIC_WRITE_START_BYTE(HTU21D_ADDR);
	ESD_IIC_WRITE_ONE_BYTE(HTU21D_RESET);
	ESD_IIC_Stop();
	
	ESD_Init_IIC(IIC_1); //IIC初始化,IIC通道1
}


void IIC_Read_HTU21D(void)  //大于50ms重复调用此函数，获取温度与湿度信息
{
	uchar dat[2]={0};
	
	HTU21D_Mode=!HTU21D_Mode;  //采集类型切换
	
	ESD_Init_IIC(IIC_2); //IIC初始化,IIC通道2
	ESD_IIC_WRITE_START_BYTE(HTU21D_ADDR+1);   //发送设备地址+读信号
	dat[0]=ESD_IIC_READ_ACK_BYTE();   //接收第一个数据
	dat[1]=ESD_IIC_READ_NACK_BYTE();   //接收第二个数据
	ESD_IIC_Stop();
	
	ESD_IIC_WRITE_START_BYTE(HTU21D_ADDR);   //发送设备地址+写信号

	if(HTU21D_Mode)	ESD_IIC_WRITE_ONE_BYTE(HTU21D_TEMP);   //开始测量温度
	else ESD_IIC_WRITE_ONE_BYTE(HTU21D_HUMI);   //开始测量湿度
	ESD_IIC_Stop();
	
	ESD_Init_IIC(IIC_1); //IIC初始化,IIC通道1
	
	if(dat[1]&0x02)  //湿度数据
	{
		dat[1]&=0xfc;   //数据最低2位清0
		HTU21D_Humi = 125.0*(dat[0]<<8|dat[1])/65536-6.0; //计算湿度并转换
		if(HTU21D_Humi>100)	HTU21D_Humi=100;
		if(HTU21D_Humi<0)	HTU21D_Humi=0;
	}
	else
	{
		dat[1]&=0xfc;   //数据最低2位清0
		HTU21D_Temp = 175.72*(dat[0]<<8|dat[1])/65536-46.85; //计算温度并转换
		if(HTU21D_Humi<-40)	HTU21D_Humi=-40;
		if(HTU21D_Humi>125)	HTU21D_Humi=125;
	}

}
