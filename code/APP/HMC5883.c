/********************************************
*********************************************************/
#include "HMC5883.h"
#include "config.h"

/**********  寄存器地址   *************/
#define	HMC5883_SlaveAddress   0x3C	  //定义器件在IIC总线中的从地址

/*------------------------------------------------------------------*/

u8 HMC_BUF[8];                         //接收数据缓存区 






/**************************************
延时5微秒(STC90C52RC@12M)
不同的工作环境,需要调整此函数，注意时钟过快时需要修改
当改用1T的MCU时,请调整此延时函数
**************************************/
void HMC5883_Delay5us()
{
    unsigned char i;

	i = 38;
	while (--i);
}

/**************************************
延时5毫秒(STC90C52RC@12M)
不同的工作环境,需要调整此函数
当改用1T的MCU时,请调整此延时函数
**************************************/
void HMC5883_Delay5ms()
{
    unsigned char i, j;

	_nop_();
	i = 32;
	j = 40;
	do
	{
		while (--j);
	} while (--i);
}

/**************************************
起始信号
**************************************/
void HMC5883_Start()
{
    HMC_SDA = 1;                    //拉高数据线
    HMC_SCL = 1;                    //拉高时钟线
    HMC5883_Delay5us();                 //延时
    HMC_SDA = 0;                    //产生下降沿
    HMC5883_Delay5us();                 //延时
    HMC_SCL = 0;                    //拉低时钟线
}

/**************************************
停止信号
**************************************/
void HMC5883_Stop()
{
    HMC_SDA = 0;                    //拉低数据线
    HMC_SCL = 1;                    //拉高时钟线
    HMC5883_Delay5us();                 //延时
    HMC_SDA = 1;                    //产生上升沿
    HMC5883_Delay5us();                 //延时
}

/**************************************
发送应答信号
入口参数:ack (0:ACK 1:NAK)
**************************************/
void HMC5883_SendACK(bit ack)
{
    HMC_SDA = ack;                  //写应答信号
    HMC_SCL = 1;                    //拉高时钟线
    HMC5883_Delay5us();                 //延时
    HMC_SCL = 0;                    //拉低时钟线
    HMC5883_Delay5us();                 //延时
}

/**************************************
接收应答信号
**************************************/
bit HMC5883_RecvACK()
{
    HMC_SCL = 1;                    //拉高时钟线
    HMC5883_Delay5us();                 //延时
    CY = HMC_SDA;                   //读应答信号
    HMC_SCL = 0;                    //拉低时钟线
    HMC5883_Delay5us();                 //延时

    return CY;
}

/**************************************
向IIC总线发送一个字节数据
**************************************/
void HMC5883_SendByte(uchar dat)
{
    uchar i;

    for (i=0; i<8; i++)         //8位计数器
    {
        dat <<= 1;              //移出数据的最高位
        HMC_SDA = CY;               //送数据口
        HMC_SCL = 1;                //拉高时钟线
        HMC5883_Delay5us();             //延时
        HMC_SCL = 0;                //拉低时钟线
        HMC5883_Delay5us();             //延时
    }
    HMC5883_RecvACK();
}

/**************************************
从IIC总线接收一个字节数据
**************************************/
uchar HMC5883_RecvByte()
{
    uchar i;
    uchar dat = 0;

    HMC_SDA = 1;                    //使能内部上拉,准备读取数据,
    for (i=0; i<8; i++)         //8位计数器
    {
        dat <<= 1;
        HMC_SCL = 1;                //拉高时钟线
        HMC5883_Delay5us();             //延时
        dat |= HMC_SDA;             //读数据               
        HMC_SCL = 0;                //拉低时钟线
        HMC5883_Delay5us();             //延时
    }
    return dat;
}

//***************************************************

void Single_Write_HMC5883(uchar REG_Address,uchar REG_data)
{
    HMC5883_Start();                  //起始信号
    HMC5883_SendByte(HMC5883_SlaveAddress);   //发送设备地址+写信号
    HMC5883_SendByte(REG_Address);    //内部寄存器地址，请参考中文pdf 
    HMC5883_SendByte(REG_data);       //内部寄存器数据，请参考中文pdf
    HMC5883_Stop();                   //发送停止信号
}

//********单字节读取内部寄存器*************************
uchar Single_Read_HMC5883(uchar REG_Address)
{  uchar REG_data;
    HMC5883_Start();                          //起始信号
    HMC5883_SendByte(HMC5883_SlaveAddress);           //发送设备地址+写信号
    HMC5883_SendByte(REG_Address);                   //发送存储单元地址，从0开始	
    HMC5883_Start();                          //起始信号
    HMC5883_SendByte(HMC5883_SlaveAddress+1);         //发送设备地址+读信号
    REG_data=HMC5883_RecvByte();              //读出寄存器数据
	HMC5883_SendACK(1);   
	HMC5883_Stop();                           //停止信号
    return REG_data; 
}
//******************************************************
//
//连续读出HMC5883内部角度数据，地址范围0x3~0x5
//
//******************************************************
void Multiple_read_HMC5883(void)
{   uchar i;
    HMC5883_Start();                          //起始信号
    HMC5883_SendByte(HMC5883_SlaveAddress);           //发送设备地址+写信号
    HMC5883_SendByte(0x03);                   //发送存储单元地址，从0x3开始	
    HMC5883_Start();                          //起始信号
    HMC5883_SendByte(HMC5883_SlaveAddress+1);         //发送设备地址+读信号
	 for (i=0; i<6; i++)                      //连续读取6个地址数据，存储中BUF
    {
        HMC_BUF[i] = HMC5883_RecvByte();          //BUF[0]存储数据
        if (i == 5)
        {
           HMC5883_SendACK(1);                //最后一个数据需要回NOACK
        }
        else
        {
          HMC5883_SendACK(0);                //回应ACK
       }
   }
    HMC5883_Stop();                          //停止信号
    HMC5883_Delay5ms();
}

//初始化HMC5883，根据需要请参考pdf进行修改****
void Init_HMC5883()
{
	  /* GPIO_init_pin(10,0);
	   GPIO_init_pin(11,0);
	   P1PU|=0x03;  //P10,P11上拉*/
     Single_Write_HMC5883(0x02,0x00);  //
}