
#include "config.h"
	
#define uint unsigned int   
#define u8 unsigned char 


extern void Init_BH1750(char n);
extern void  Single_Write_BH1750(char Pass,unsigned char REG_Address);//单个写入数据
extern unsigned char Single_Read_BH1750(char Pass,unsigned char REG_Address);//单个读取内部寄存器数据
extern void  Multiple_Read_BH1750(char Pass); //连续的读取内部寄存器数据
//------------------------------------								
extern void Delay5us();
extern void Delay5ms();
extern void BH1750_Start(char Pass);                    //起始信号
extern void BH1750_Stop(char Pass);                     //停止信号
extern void BH1750_SendACK(char Pass,bit ack);           //应答ACK
extern bit  BH1750_RecvACK(char Pass);                  //读ack
extern void BH1750_SendByte(char Pass,unsigned char dat);         //IIC单个字节写
extern unsigned char BH1750_RecvByte(char Pass);                 //IIC单个字节读


extern unsigned char BH1750_BUF[8][8];


