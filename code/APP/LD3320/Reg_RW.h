#ifndef REG_RW_H
#define REG_RW_H



sbit SDCK=P1^5;   //SPI 时钟信号
sbit SDO=P1^4;    //SPI 数据输出
sbit SDI=P1^3;    //SPI 数据输入
sbit SCS=P1^2;    //芯片片选信号
sbit WR=P1^1;
sbit RSTB=P1^0;	 /*复位端口*/

//函数声明
void LD_WriteReg( unsigned char address, unsigned char dataout );
unsigned char LD_ReadReg( unsigned char address );
void Delay5us();

#endif
