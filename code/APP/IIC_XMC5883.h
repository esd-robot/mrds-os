#ifndef __IIC_XMC5883L_H
#define __IIC_XMC5883L_H

#include "config.h"

#define QMC5883L	
//#define HMC5883L	
#define _5883_8G_mag_transition(mag_value)    ((float)mag_value / 3000;
#define _5883_2G_mag_transition(mag_value)    ((float)mag_value / 12000;
#define _FAIL		1
#define _SUCCESS	0

#ifdef QMC5883L
	#ifdef HMC5883L
		#error //同时define了QMC5883L、HMC5883L
	#endif
#endif

#ifdef HMC5883L
	#define MAG_ADDRESS				0x1E

		#define MAG_DATA_REGISTER		0x03
		#define MAG_X_DATA_REGISTER		0x03
		#define MAG_Y_DATA_REGISTER		0x07
		#define MAG_Z_DATA_REGISTER		0x05

		#define ConfigRegA				0x00

			// ConfigRegA valid Data output rates for 5883L
			#define DataOutputRate_0_75HZ	0x00
			#define DataOutputRate_1_5HZ	0x01
			#define DataOutputRate_3HZ		0x02
			#define DataOutputRate_7_5HZ	0x03
			#define DataOutputRate_15HZ		0x04
			#define DataOutputRate_30HZ		0x05
			#define DataOutputRate_75HZ		0x06
			#define DataOutputRate_Max		DataOutputRate_75HZ

			// ConfigRegA valid sample averaging for 5883L
			#define SampleAveraging_1		0x00
			#define SampleAveraging_2		0x01
			#define SampleAveraging_4		0x02
			#define SampleAveraging_8		0x03
			#define Sample_MAX				SampleAveraging_8

			#define NormalOperation			0x00
			#define PositiveBiasConfig		0x01
			#define NegativeBiasConfig		0x02

		#define ConfigRegB				0x01

			#define Full_Scale_0_88G		0x00
			#define Full_Scale_1_3G			0x01
			#define Full_Scale_1_9G			0x02
			#define Full_Scale_2_5G			0x03
			#define Full_Scale_4_0G			0x04
			#define Full_Scale_4_7G			0x05
			#define Full_Scale_5_6G			0x06
			#define Full_Scale_8_1G			0x07

		#define ModeRegister			0x02

			#define ContinuousConversion	0x00
			#define SingleConversion		0x01

		#define StatusReg				0x09	

		#define IDRegA					0x0A
		#define IDRegB					0x0B
		#define IDRegC					0x0C
#endif

#ifdef QMC5883L
//	#define MAG_ADDRESS 0x0C	//SI = GND
	#define MAG_ADDRESS 0x0D	//SI = VDD

		#define MAG_DATA_REGISTER		0x00
		#define MAG_X_DATA_REGISTER		0x00
		#define MAG_Y_DATA_REGISTER		0x02
		#define MAG_Z_DATA_REGISTER		0x04
		#define MAG_TEMP_DATA_REGISTER	0x07

		#define ConfigReg1				0x09		//状态寄存器
			//采样频率
			// ConfigReg1 valid Data output rates for 5883L
			#define DataOutputRate_10HZ		0x00
			#define DataOutputRate_50HZ		0x01
			#define DataOutputRate_100HZ	0x02
			#define DataOutputRate_200HZ	0x03
			#define DataOutputRate_Max		DataOutputRate_200HZ

			#define SampleOverRatio_512		0x00
			#define SampleOverRatio_256		0x01
			#define SampleOverRatio_128		0x02
			#define SampleOverRatio_64		0x03
			#define Sample_MAX				SampleOverRatio_256
			//量程
			#define Full_Scale_2G			0x00
			#define Full_Scale_8G			0x01

			#define StandbyConversion		0x00 
			#define ContinuousConversion	0x01

		#define ConfigReg2				0x0A

			#define Enable_Interrupt_PIN	0x00
			#define Disable_Interrupt_PIN	0x01

			#define ROL_PNT_Normal			0x00
			#define ROL_PNT_Enable			0x01

			#define SOFT_RST_Normal			0x00
			#define SOFT_RST_Reset			0x01

		#define StatusReg				0x06

		#define Period_FBR				0x0b

		#define IDReg					0x0d 	//可读到0xFF

		extern int16 QMC5883L_Temp;
#endif
extern int16 mag_x,mag_y,mag_z;
extern int16 QMC5883L_Temp;
void xmc5883lInit(void);
void xmc5883lRead(void);
void GetMrgAngle(void);
extern double MAG_Angle;
#endif
//------------------End of File----------------------------

