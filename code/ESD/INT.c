#include "INT.h"
void INT_init(uint8 int_n,uint8 mode)
{
	if(0 == int_n)
	{
		IT0 = mode;
		EX0 = 1; 		//使能INT0中断
	}
	if(1 == int_n)
	{
		IT1 = mode;
		EX1 = 1; 		//使能INT1中断
	}	
	if(2 == int_n)
	{
		INTCLKO |= 1<<4;	//使能INT2中断
	}	
	if(3 == int_n)
	{
		INTCLKO |= 1<<5;	//使能INT3中断
	}	
	if(4 == int_n)
	{
		INTCLKO |= 1<<6;	//使能INT4中断
	}
}