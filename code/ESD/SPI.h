#ifndef		__SPI_H
#define		__SPI_H
#include "config.h"

void SPI_Init(unsigned char spi_n);
void SPI_Deinit(void);
void SPI_WriteByte(unsigned char out);
unsigned char SPI_ReadByte(void);
void SPI_WriteMultiBytes(unsigned char *tx_ptr, unsigned int n);
void SPI_ReadMultiBytes(unsigned char *rx_ptr, unsigned int n);
unsigned char ESD_SPI_RW(unsigned char dat);
#endif