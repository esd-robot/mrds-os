#include "PWM.h"
//捕获比较模式寄存器
const uint32 PWM_CCMR_ADDR[] = {0x7efec8, 0x7efec9, 0x7efeca ,0x7efecb, 
								0x7efee8, 0x7efee9, 0x7efeea, 0x7efeeb};
//捕获比较使能寄存器
const uint32 PWM_CCER_ADDR[] = {0x7efecc, 0x7efecd, 
								0x7efeec ,0x7efeed};
//控制寄存器,高8位地址  低8位地址 + 1即可
const uint32 PWM_CCR_ADDR[] = {0x7efed5, 0x7efed7, 0x7efed9, 0x7efedb,
							   0x7efef5, 0x7efef7, 0x7efef9, 0x7efefb};
	
							   //控制寄存器,高8位地址  低8位地址 + 1即可
const uint32 PWM_ARR_ADDR[] = {0x7efed2,0x7efef2};
void PWM_init(PWMCH_enum pwmch,uint32 freq, uint32 duty)
{
	
	uint32 match_temp;
	uint32 period_temp; 
	uint16 freq_div = 0;
	
	
	P_SW2 |= 0x80;
	
	//GPIO需要设置为推挽输出
//	switch(pwmch);


	//分频计算，周期计算，占空比计算
	freq_div = (sys_clk / freq) >> 16;							//多少分频
	period_temp = sys_clk / freq ;			
	period_temp = period_temp / (freq_div + 1) - 1;				//周期

	if(duty != PWM_DUTY_MAX)
	{
		match_temp = period_temp * ((float)duty / PWM_DUTY_MAX);	// 占空比			
	}
	else
	{
		match_temp = period_temp + 1;								// duty为100%
	}

	
	if(PWMB_CH1_P20 <= pwmch)				//PWM5-8
	{
		//通道选择，引脚选择
		PWMB_ENO |= (1 << ((2 * ((pwmch >> 4) - 4))));					//使能通道	
		PWMB_PS |= ((pwmch & 0x03) << ((2 * ((pwmch >> 4) - 4))));		//输出脚选择
		
		// 配置通道输出使能和极性	
		(*(unsigned char volatile far *) (PWM_CCER_ADDR[pwmch>>5])) |= (uint8)(1 << (((pwmch >> 4) & 0x01) * 4));
		
		//设置预分频
		PWMB_PSCRH = (uint8)(freq_div>>8);
		PWMB_PSCRL = (uint8)freq_div;
		
		PWMB_BKR = 0x80; 	//主输出使能 相当于总开关
		PWMB_CR1 = 0x01;	//PWM开始计数
	}
	else
	{
		PWMA_ENO &= ~(0x03<<((pwmch >> 4) * 2));	//先清零
		PWMA_ENO |= (1 << (pwmch & 0x01)) << ((pwmch >> 4) * 2);	//使能通道	
		PWMA_PS  |= ((pwmch & 0x07) >> 1) << ((pwmch >> 4) * 2);    //输出脚选择
		
		// 配置通道输出使能和极性	
		(*(unsigned char volatile far *) (PWM_CCER_ADDR[pwmch>>5])) &= 0xf0>>(4*((pwmch >> 4) & 0x01)); //先清0
		(*(unsigned char volatile far *) (PWM_CCER_ADDR[pwmch>>5])) |= (1 << ((pwmch & 0x01) * 2 + ((pwmch >> 4) & 0x01) * 4));//后写入

		
		//设置预分频
		PWMA_PSCRH = (uint8)(freq_div>>8);
		PWMA_PSCRL = (uint8)freq_div;

		PWMA_BKR = 0x80; 	// 主输出使能 相当于总开关
		PWMA_CR1 = 0x01;	//PWM开始计数
	}
	
	//周期
	(*(unsigned char volatile far *) (PWM_ARR_ADDR[pwmch>>6])) = (uint8)(period_temp>>8);		//高8位
	(*(unsigned char volatile far *) (PWM_ARR_ADDR[pwmch>>6] + 1)) = (uint8)period_temp;		//低8位

	//设置捕获值|比较值
	(*(unsigned char volatile far *) (PWM_CCR_ADDR[pwmch>>4]))		= match_temp>>8;			//高8位
	(*(unsigned char volatile far *) (PWM_CCR_ADDR[pwmch>>4] + 1))  = (uint8)match_temp;		//低8位
	
	//功能设置
	(*(unsigned char volatile far *) (PWM_CCMR_ADDR[pwmch>>4])) |= 0x06<<4;		//设置为PWM模式1
	(*(unsigned char volatile far *) (PWM_CCMR_ADDR[pwmch>>4])) |= 1<<3;		//开启PWM寄存器的预装载功
//	P_SW2 &= 0x7F;

}
void PWM_change(PWMCH_enum pwmch, uint32 duty)
{
	uint32 match_temp;
	uint32 arr = ((*(unsigned char volatile far *) (PWM_ARR_ADDR[pwmch>>6]))<<8) | (*(unsigned char volatile far *) (PWM_ARR_ADDR[pwmch>>6] + 1 ));

//	P_SW2 |= 0x80;
	if(PWMB_CH1_P20 <= pwmch)				//PWM5-8
	{
		//通道选择，引脚选择
		PWMB_ENO |= (1 << ((2 * ((pwmch >> 4) - 4))));					//使能通道	
		PWMB_PS |= ((pwmch & 0x03) << ((2 * ((pwmch >> 4) - 4))));		//输出脚选择

	}
	else
	{
		PWMA_ENO &= ~(0x03<<((pwmch >> 4) * 2));	//先清零
		PWMA_ENO |= (1 << (pwmch & 0x01)) << ((pwmch >> 4) * 2);	//使能通道	
		PWMA_PS  |= ((pwmch & 0x07) >> 1) << ((pwmch >> 4) * 2);    //输出脚选择
		
		// 配置通道输出使能和极性	
		(*(unsigned char volatile far *) (PWM_CCER_ADDR[pwmch>>5])) &= 0xf0>>(4*((pwmch >> 4) & 0x01)); //先清0
		(*(unsigned char volatile far *) (PWM_CCER_ADDR[pwmch>>5])) |= (1 << ((pwmch & 0x01) * 2 + ((pwmch >> 4) & 0x01) * 4));//后写入

	}
	
	if(duty != PWM_DUTY_MAX)
	{
		match_temp = arr * ((float)duty/PWM_DUTY_MAX);				//占空比
	}
	else
	{
		match_temp = arr + 1;
	}
	//设置捕获值|比较值
	(*(unsigned char volatile far *) (PWM_CCR_ADDR[pwmch>>4]))		= match_temp>>8;			//高8位
	(*(unsigned char volatile far *) (PWM_CCR_ADDR[pwmch>>4] + 1))  = (uint8)match_temp;		//低8位
	
//	P_SW2 &= ~0x80;
	
}
