#include "PIT.h"

unsigned int T0_cnt=0;
unsigned int T1_cnt=0;
unsigned int T2_cnt=0;
unsigned int T3_cnt=0;
unsigned int T4_cnt=0;


void PIT_init_ms(uint8 tim_n,uint16 time_ms)
{
	uint16 temp;
	temp = (uint16)65536 - (uint16)(sys_clk / (12 * (1000 / time_ms)));	
	if(tim_n == 0)
	{
		TMOD |= 0x00; 	
		TL0 = temp; 	
		TH0 = temp >> 8;
		TR0 = 1; 		// 启动定时器
		ET0 = 1; 		// 使能定时器中断
	}
	else if(tim_n == 1)
	{
		TMOD |= 0x00; 
		TL1 = temp; 	
		TH1 = temp >> 8;
		TR1 = 1; // 启动定时器
		ET1 = 1; // 使能定时器中断
	}
	else if(tim_n == 2)
	{
		T2L = temp; 	
		T2H = temp >> 8;
		AUXR |= 0x10; // 启动定时器
		IE2 |= 0x04; // 使能定时器中断
	}
	else if(tim_n == 3)
	{
		T3L = temp; 	
		T3H = temp >> 8;
		T4T3M |= 0x08; // 启动定时器
		IE2 |= 0x20; // 使能定时器中断
	}
	else if(tim_n == 4)
	{
		T4L = temp; 	
		T4H = temp >> 8;
		T4T3M |= 0x80; // 启动定时器
		IE2 |= 0x40; // 使能定时器中断
	}
}
void PIT_init_us(uint8 tim_n,uint16 time_us)
{
	uint16 temp;
	temp = (uint16)65536 - (uint16)(sys_clk/1000 / (12 * (1000 / time_us)));	
	if(tim_n == 0)
	{
		TMOD |= 0x00; 	
		TL0 = temp; 	
		TH0 = temp >> 8;
		TR0 = 1; 		// 启动定时器
		ET0 = 1; 		// 使能定时器中断
	}
	else if(tim_n == 1)
	{
		TMOD |= 0x00; 
		TL1 = temp; 	
		TH1 = temp >> 8;
		TR1 = 1; // 启动定时器
		ET1 = 1; // 使能定时器中断
	}
	else if(tim_n == 2)
	{
		T2L = temp; 	
		T2H = temp >> 8;
		AUXR |= 0x10; // 启动定时器
		IE2 |= 0x04; // 使能定时器中断
	}
	else if(tim_n == 3)
	{
		T3L = temp; 	
		T3H = temp >> 8;
		T4T3M |= 0x08; // 启动定时器
		IE2 |= 0x20; // 使能定时器中断
	}
	else if(tim_n == 4)
	{
		T4L = temp; 	
		T4H = temp >> 8;
		T4T3M |= 0x80; // 启动定时器
		IE2 |= 0x40; // 使能定时器中断
	}
}
void PIT_init_encoder(uint8 tim_n)
{
	switch(tim_n)
	{
		case 0://使用P34引脚
		{
			TL0 = 0; 
			TH0 = 0; 
			TMOD |= 0x04; //外部计数模式
			TR0 = 1; //启动定时器
			break;
		}	
		case 1://使用P35引脚
		{
			TL1 = 0x00;
			TH1 = 0x00;
			TMOD |= 0x40; // 外部计数模式
			TR1 = 1; // 启动定时器
			break;
		}	
		case 2://使用P12引脚
		{
			T2L = 0x00;
			T2H = 0x00;
			AUXR |= 0x18; // 设置外部计数模式并启动定时器
			break;
		}		
		case 3://使用P04引脚
		{
			T3L = 0; 
			T3H = 0;
			T4T3M |= 0x0c; // 设置外部计数模式并启动定时器
			break;
		}		
		case 4://使用P06引脚
		{
			T4L = 0;
			T4H = 0;
			T4T3M |= 0xc0; // 设置外部计数模式并启动定时器
			break;
		}	
	}	
}
uint16 PIT_count_get(uint8 tim_n)
{
	uint16 dat = 0;	
	switch(tim_n)
	{
		case 0:
		{
			dat = (uint16)TH0 << 8;
			dat = ((uint8)TL0) | dat;
			break;
		}
		case 1:
		{
			dat = (uint16)TH1 << 8;
			dat = ((uint8)TL1) | dat;
			break;
		}
		case 2:
		{
			dat = (uint16)T2H << 8;
			dat = ((uint8)T2L) | dat;
			break;
		}
		case 3:
		{
			dat = (uint16)T3H << 8;
			dat = ((uint8)T3L) | dat;	
			break;
		}
		case 4:
		{
			dat = (uint16)T4H << 8;
			dat = ((uint8)T4L) | dat;
			break;
		}	
	}
	return dat;
}
void PIT_count_clean(uint8 tim_n)
{	
	switch(tim_n)
	{
		case 0:
		{
			TR0 = 0;
			TH0 = 0;
			TL0 = 0;
			TR0 = 1;
			break;
		}
		case 1:
		{
			TR1 = 0;
			TH1 = 0;
			TL1 = 0;
			TR1 = 1;
			break;
		}
		case 2:
		{
			AUXR &= ~(1<<4);
			T2H = 0;
			T2L = 0;
			AUXR |= 1<<4;
			break;
		}
		case 3:
		{
			T4T3M &= ~(1<<3);
			T3H = 0;
			T3L = 0;
			T4T3M |= (1<<3);
			break;
		}
		case 4:
		{
			T4T3M &= ~(1<<7);
			T4H = 0;
			T4L = 0;
			T4T3M |= (1<<7);
			break;
		}
	}
}