#ifndef		__UART_H
#define		__UART_H
#include "config.h"

extern unsigned char UART1_OK;
extern unsigned char UART2_OK;
extern unsigned char UART3_OK;
extern unsigned char UART4_OK;

void UART_init(unsigned char pin,unsigned long btl,unsigned char n,unsigned char isr);
void UART_Send_byte(unsigned char pin,unsigned char c);  //调用串口模块发送单个字符
void UART_Send_buff(unsigned char pin,unsigned char *pt,uint16 len);
void UART_Send_string(unsigned char pin,unsigned char *pt); //调用串口模块发送字符串
void UART_Send_int(unsigned char pin,unsigned char *pa,long int num,unsigned char *pb);
void UART_Send_float(unsigned char pin,unsigned char *ps,float num,unsigned char *pe);
#endif