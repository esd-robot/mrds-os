/********************************************
*********************************************************/
#include "IIC.h"
#include "config.h"

/**********  寄存器地址   *************/


void ESD_IIC_Wait()
{
	while(!(I2CMSST & 0x40));
   I2CMSST &= ~0x40;
}

/**************************************
起始信号
**************************************/
void ESD_IIC_Start()  //发送起始信号
{
	I2CMSCR = 0X01;
	ESD_IIC_Wait();
}
/**************************************
向IIC总线发送一个字节数据
**************************************/
void ESD_IIC_Send_data(uchar dat)
{
    I2CTXD = dat;                               //写数据到数据缓冲区
    I2CMSCR = 0x02;                             //发送SEND命令
    ESD_IIC_Wait();
}
/**************************************
接收应答信号
**************************************/
void ESD_IIC_Recv_ack()
{
    I2CMSCR = 0x03;                             //发送读ACK命令
    ESD_IIC_Wait();
}
/**************************************
从IIC总线接收一个字节数据
**************************************/
uchar ESD_IIC_Recv_data()
{
    I2CMSCR = 0x04;                             //发送RECV命令
    ESD_IIC_Wait();
    return I2CRXD;
}

/**************************************
发送应答信号0
**************************************/
void ESD_IIC_Send_ack()
{
    I2CMSST = 0x00;                             //设置ACK信号
    I2CMSCR = 0x05;                             //发送ACK命令
    ESD_IIC_Wait();
}
/**************************************
发送应答信号1
**************************************/
void ESD_IIC_Send_nack()
{
    I2CMSST = 0x01;                             //设置NAK信号
    I2CMSCR = 0x05;                             //发送ACK命令
    ESD_IIC_Wait();
}

/**************************************
停止信号
**************************************/
void ESD_IIC_Stop()
{
    I2CMSCR = 0x06;                             //发送STOP命令
    ESD_IIC_Wait();
}

/**************************************
写完整单个数据 = 起始+发送数据+接收ACK组合
**************************************/
void ESD_IIC_WRITE_START_BYTE(uchar dat)
{
		I2CTXD = dat;                               //写数据到数据缓冲区
    I2CMSCR = 0x09;                             //发送STOP命令
    ESD_IIC_Wait();
}

/**************************************
写单个数据 = 发送数据+接收ACK组合
**************************************/
void ESD_IIC_WRITE_ONE_BYTE(uchar dat)
{
		I2CTXD = dat;                               //写数据到数据缓冲区
    I2CMSCR = 0x0A;                             //发送STOP命令
    ESD_IIC_Wait();
}

/**************************************
接收单个数据 = 接收数据+返回ACK
**************************************/
uchar ESD_IIC_READ_ACK_BYTE()
{
    I2CMSCR = 0x0B;                             //发送STOP命令
    ESD_IIC_Wait();
    return I2CRXD;
}

/**************************************
接收单个数据 = 接收数据+返回NACK
**************************************/
uchar ESD_IIC_READ_NACK_BYTE()
{
    I2CMSCR = 0x0C;                             //发送STOP命令
    ESD_IIC_Wait();
    return I2CRXD;
}

/*---------------------功能函数---------------------------------*/

//**************************************
/*功　　能:	    将多个字节写入指定设备 指定寄存器
输入	dev  目标设备地址
		reg	  寄存器地址
		length 要写的字节数
		*dat  将要写的数据的首地址
返回   返回是否成功*/
//**************************************
void ESD_Write_IIC(u8 dev, u8 reg, u8 length, u8* dat)
{
		uchar count = 0;
	
    ESD_IIC_Start();                  //起始信号
    ESD_IIC_Send_data(dev<<1);   //发送设备地址+写信号
		ESD_IIC_Recv_ack();
    ESD_IIC_Send_data(reg);    //内部寄存器地址，
		ESD_IIC_Recv_ack();
		for(count=0;count<length;count++)
		{
			ESD_IIC_Send_data(dat[count]); 
			ESD_IIC_Recv_ack(); 
	  }
    ESD_IIC_Stop();                   //发送停止信号
}

//*********************************************************
/*功　　能:	    读取指定设备 指定寄存器的 length个值
输入	dev  目标设备地址
		reg	  寄存器地址
		length 要读的字节数
		*dat  读出的数据将要存放的指针
返回   读出来的字节数量*/
//*********************************************************
void ESD_Read_IIC(u8 dev, u8 reg, u8 length, u8 *dat)
{  
		uchar count = 0;
		
		ESD_IIC_Start();                          //起始信号
		ESD_IIC_Send_data(dev<<1);   //发送写命令
		ESD_IIC_Recv_ack();
		ESD_IIC_Send_data(reg);         //发送读信号
		ESD_IIC_Recv_ack();

		ESD_IIC_Start();
		ESD_IIC_Send_data((dev<<1)+1);   //进入接收模式	
		ESD_IIC_Recv_ack();

		for(count=0;count<length;count++)
		{
			dat[count] = ESD_IIC_Recv_data();          //BUF[0]存储0x32地址中的数据
			if(count!=length-1) ESD_IIC_Send_ack();  //不是最后发ack标志
			else ESD_IIC_Send_nack(); //最后一个数据发nack标志
			
		}
		ESD_IIC_Stop();                          //停止信号

}


//初始化IIC，主机模式，并切换IIC复用端口
void ESD_Init_IIC(IICN_enum iic_n)
{						
	
    switch(iic_n)
    {
        case 0:
        {
            //切换到第1组I2C	(P1.5/SCL, P1.4/SDA)  
            P_SW2 &= ~(0X30);	//I2C_S0=0 I2C_S1=0
					  P1PU |= 0X30;  //IO开漏+上拉
					  P1M0 |=0X30;
						P1M1 |=0X30;
                               
        }break;
        case 1:
        {
           //切换到第2组I2C	(P2.5/SCL, P2.4/SDA) 
            P_SW2 &= ~(0x30);	 //I2C_S0=1 I2C_S1=0
            P_SW2 |= 0x10;				
						P2PU |= 0X30;	//IO开漏+上拉
						P2M0 |=0X30;
						P2M1 |=0X30;
        }break;
        case 2:
        {
           //切换到第3组I2C	(P7.7/SCL, P7.6/SDA) 
            P_SW2 &= ~(0x30);	//I2C_S0=0 I2C_S1=1
            P_SW2 |= 0x20;			
						P7PU |= 0XC0;	//IO开漏+上拉
					  P7M0 |=0XC0;
						P7M1 |=0XC0;
        }break;
        case 3:
        {
           //切换到第4组I2C	(P3.2/SCL, P3.3/SDA)                                  
            P_SW2 |= (0X30);//I2C_S0=1 I2C_S1=1
						P3PU |= 0X0C;	//IO开漏+上拉
						P3M0 |=0X0C;
						P3M1 |=0X0C;
        }break;
    }
	
	I2CCFG = 0xE0;                    //使能I2C主机模式
  I2CCFG |= 13;                    //IIC速度   28-200k   13-400k
	I2CMSST = 0x00;
	

}
