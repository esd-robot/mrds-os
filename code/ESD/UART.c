#include "UART.h"

uchar UART1_OK=0;
uchar UART2_OK=0;
uchar UART3_OK=0;
uchar UART4_OK=0;

void UART_init(unsigned char pin,unsigned long btl,unsigned char n,unsigned char isr)
{
	if(pin==1)		//串口1
	{
			P_SW1 &= ~0xc0;
			switch(n)
			{
				case 1:P_SW1 |= 0x00;break;   //串口1引脚切换到 P30.P31
				case 2:P_SW1 |= 0x40;break;   //串口1引脚切换到 P36.P37
				case 3:P_SW1 |= 0x80;break;   //串口1引脚切换到 P16.P17
				case 4:P_SW1 |= 0xc0;break;   //串口1引脚切换到 P43.P44
			}
		  SCON = 0x50;    //串口1工作模式为0，8位数据模式
		  AUXR &= 0xFE;   //串口1选择定时器1作为波特率发生器
		  TMOD |= 0x00;    
			TL1 = (65536 - sys_clk / btl / 4); //定时器2装初值
			TH1 = (65536 - sys_clk / btl / 4) >> 8; 
			TR1 = 1;																 //启动定时器1
			AUXR |= 0x40;                            //定时器1，1T模式
			if(isr==1)
			{ES = 1;}                                  //使能串口1中断
	}
	if(pin ==2) 
	{		
		
			if(n==1)	P_SW2 &= ~0x01;   //串口2引脚切换到 P10.P11
			else	P_SW2 |= 0x01;   //串口2引脚切换到 P46.P47
		  S2CON = 0x50;    //串口2工作模式为0，8位数据模式
			S2CFG = 0x01;    //
			T2L = (65536 - sys_clk / btl / 4); //定时器2装初值
			T2H = (65536 - sys_clk / btl / 4) >> 8; 
			T2x12 =1;
			T2R=1;		//启动定时器2，1T模式
			if(isr==1)
			{ES2=1;}                                  //使能串口2中断
	}
	if(pin ==3) 
	{		
			if(n==1)	P_SW2 &= ~0x02;   //串口3引脚切换到 P00.P01
			else	P_SW2 |= 0x02;   //串口3引脚切换到 P50.P51
		  S3CON = 0x50;    //串口3选择定时器3作为波特率发生器
			T3L = (65536 - sys_clk / btl / 4); //定时器3装初值
			T3H = (65536 - sys_clk / btl / 4) >> 8; 
			T3x12 =1;
			T3R=1;		//启动定时器3，1T模式
			if(isr==1)
			{ES3=1;}                                  //使能串口3中断
	}
	if(pin ==4) 
	{			
			if(n==1)	P_SW2 &= ~0x04;   //串口4引脚切换到 P02.P03
			else	P_SW2 |= 0x04;   //串口4引脚切换到 P52.P53
		  S4CON = 0x50;    //串口4选择定时器4作为波特率发生器
			T4L = (65536 - sys_clk / btl / 4); //定时器4装初值
			T4H = (65536 - sys_clk / btl / 4) >> 8; 
			T4x12 =1;
			T4R=1;		//启动定时器4，1T模式
			if(isr==1)
			{ES4=1;}                                  //使能串口4中断
	}

}
void UART_Send_byte(unsigned char pin,unsigned char c)  //调用串口模块发送单个字符
{
	if(pin==1)
	{
		while(UART1_OK);
		UART1_OK =1;
    SBUF = c;	
	}
	if(pin==2)
	{
		while(UART2_OK);
		UART2_OK =1;
		S2BUF = c;	
	}
	if(pin==3)
	{
		while(UART3_OK);
		UART3_OK =1;
		S3BUF = c;
	}
	if(pin==4)
	{
		while(UART4_OK);
		UART4_OK =1;
		S4BUF = c;	
	}
}
void UART_Send_string(unsigned char pin,unsigned char *pt) //调用串口模块发送字符串
{
	while(*pt != '\0')
	{
		UART_Send_byte(pin,*pt++);
	}
}
void UART_Send_int(unsigned char pin,unsigned char *pa,long int num,unsigned char *pb)
{
	unsigned char k;
	
	while(*pa != '\0')
	{
		UART_Send_byte(pin,*pa++);
	}
	if(num == 0)	UART_Send_byte(pin,'0');
	else
	{
		if(num < 0)	UART_Send_byte(pin,'-');
		k=(int)floor(log10(labs(num)))+1;
		switch(k)
		{
			case 1:
				UART_Send_byte(pin,labs(num)/1%10+0x30);
				break;
			case 2:
				UART_Send_byte(pin,labs(num)/10%10+0x30);
				UART_Send_byte(pin,labs(num)/1%10+0x30);
				break;
			case 3:
				UART_Send_byte(pin,labs(num)/100%10+0x30);
				UART_Send_byte(pin,labs(num)/10%10+0x30);
				UART_Send_byte(pin,labs(num)/1%10+0x30);
				break;
			case 4:
				UART_Send_byte(pin,labs(num)/1000%10+0x30);
				UART_Send_byte(pin,labs(num)/100%10+0x30);
				UART_Send_byte(pin,labs(num)/10%10+0x30);
				UART_Send_byte(pin,labs(num)/1%10+0x30);
				break;
			case 5:
				UART_Send_byte(pin,labs(num)/10000%10+0x30);
				UART_Send_byte(pin,labs(num)/1000%10+0x30);
				UART_Send_byte(pin,labs(num)/100%10+0x30);
				UART_Send_byte(pin,labs(num)/10%10+0x30);
				UART_Send_byte(pin,labs(num)/1%10+0x30);
				break;
			case 6:
				UART_Send_byte(pin,labs(num)/100000%10+0x30);
				UART_Send_byte(pin,labs(num)/10000%10+0x30);
				UART_Send_byte(pin,labs(num)/1000%10+0x30);
				UART_Send_byte(pin,labs(num)/100%10+0x30);
				UART_Send_byte(pin,labs(num)/10%10+0x30);
				UART_Send_byte(pin,labs(num)/1%10+0x30);
				break;
			case 7:
				UART_Send_byte(pin,labs(num)/1000000%10+0x30);
				UART_Send_byte(pin,labs(num)/100000%10+0x30);
				UART_Send_byte(pin,labs(num)/10000%10+0x30);
				UART_Send_byte(pin,labs(num)/1000%10+0x30);
				UART_Send_byte(pin,labs(num)/100%10+0x30);
				UART_Send_byte(pin,labs(num)/10%10+0x30);
				UART_Send_byte(pin,labs(num)/1%10+0x30);
				break;
			case 8:
				UART_Send_byte(pin,labs(num)/10000000%10+0x30);
				UART_Send_byte(pin,labs(num)/1000000%10+0x30);
				UART_Send_byte(pin,labs(num)/100000%10+0x30);
				UART_Send_byte(pin,labs(num)/10000%10+0x30);
				UART_Send_byte(pin,labs(num)/1000%10+0x30);
				UART_Send_byte(pin,labs(num)/100%10+0x30);
				UART_Send_byte(pin,labs(num)/10%10+0x30);
				UART_Send_byte(pin,labs(num)/1%10+0x30);
				break;
			case 9:
				UART_Send_byte(pin,labs(num)/100000000%10+0x30);
				UART_Send_byte(pin,labs(num)/10000000%10+0x30);
				UART_Send_byte(pin,labs(num)/1000000%10+0x30);
				UART_Send_byte(pin,labs(num)/100000%10+0x30);
				UART_Send_byte(pin,labs(num)/10000%10+0x30);
				UART_Send_byte(pin,labs(num)/1000%10+0x30);
				UART_Send_byte(pin,labs(num)/100%10+0x30);
				UART_Send_byte(pin,labs(num)/10%10+0x30);
				UART_Send_byte(pin,labs(num)/1%10+0x30);
				break;
			case 10:
				UART_Send_byte(pin,labs(num)/1000000000%10+0x30);
				UART_Send_byte(pin,labs(num)/100000000%10+0x30);
				UART_Send_byte(pin,labs(num)/10000000%10+0x30);
				UART_Send_byte(pin,labs(num)/1000000%10+0x30);
				UART_Send_byte(pin,labs(num)/100000%10+0x30);
				UART_Send_byte(pin,labs(num)/10000%10+0x30);
				UART_Send_byte(pin,labs(num)/1000%10+0x30);
				UART_Send_byte(pin,labs(num)/100%10+0x30);
				UART_Send_byte(pin,labs(num)/10%10+0x30);
				UART_Send_byte(pin,labs(num)/1%10+0x30);
				break;
		}
	}
	
	while(*pb != '\0')
	{
		UART_Send_byte(pin,*pb++);
	}
}
void UART_Send_float(unsigned char pin,unsigned char *ps,float num,unsigned char *pe)
{
	unsigned char k;
	unsigned long int l;
	
	l= floor(fabs(num));
	
	while(*ps != '\0')
	{
		UART_Send_byte(pin,*ps++);
	}
	if(num < 0)	UART_Send_byte(pin,'-');
	
	if(l==0)	UART_Send_byte(pin,'0');
	
	k=(int) floor(log10((unsigned long)fabs(num)))+1;
	switch(k)
	{
		case 1:
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1)%10+0x30);
			break;
		case 2:
			UART_Send_byte(pin,(unsigned long)(fabs(num)/10)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1)%10+0x30);
			break;
		case 3:
			UART_Send_byte(pin,(unsigned long)(fabs(num)/100)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/10)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1)%10+0x30);
			break;
		case 4:
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/100)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/10)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1)%10+0x30);
			break;
		case 5:
			UART_Send_byte(pin,(unsigned long)(fabs(num)/10000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/100)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/10)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1)%10+0x30);
			break;
		case 6:
			UART_Send_byte(pin,(unsigned long)(fabs(num)/100000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/10000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/100)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/10)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1)%10+0x30);
			break;
		case 7:
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1000000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/100000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/10000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/100)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/10)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1)%10+0x30);
			break;
		case 8:
			UART_Send_byte(pin,(unsigned long)(fabs(num)/10000000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1000000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/100000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/10000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/100)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/10)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1)%10+0x30);
			break;
		case 9:
			UART_Send_byte(pin,(unsigned long)(fabs(num)/100000000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/10000000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1000000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/100000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/10000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/100)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/10)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1)%10+0x30);
			break;
		case 10:
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1000000000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/100000000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/10000000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1000000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/100000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/10000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/100)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/10)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)/1)%10+0x30);
			break;
	}
	UART_Send_byte(pin,'.');
	
	l = (unsigned long)(fabs(num)*10)%10*1 + (unsigned long)(fabs(num)*100)%10*10 + (unsigned long)(fabs(num)*1000)%10*100 + (unsigned long)(fabs(num)*10000)%10*1000;
	
	l = (int) floor(log10((unsigned long)fabs(l)))+1;
	switch(l)
	{
		case 0:
			UART_Send_byte(pin,'0');
		break;
		case 1:
			UART_Send_byte(pin,(unsigned long)(fabs(num)*10)%10+0x30);
			break;
		case 2:
			UART_Send_byte(pin,(unsigned long)(fabs(num)*10)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)*100)%10+0x30);
			break;
		case 3:
			UART_Send_byte(pin,(unsigned long)(fabs(num)*10)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)*100)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)*1000)%10+0x30);
			break;
		case 4:
			UART_Send_byte(pin,(unsigned long)(fabs(num)*10)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)*100)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)*1000)%10+0x30);
			UART_Send_byte(pin,(unsigned long)(fabs(num)*10000)%10+0x30);
			break;
	}	
	while(*pe != '\0')
	{
		UART_Send_byte(pin,*pe++);
	}
}