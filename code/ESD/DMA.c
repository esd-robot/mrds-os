/**********************************************************
    不建议更改此文档，相关函数功能请查看相关头文件定义
**********************************************************/
#include "DMA.h"


void DMA_TXD_init(unsigned char pin,unsigned int length,uchar *DAT)    //DMA发送串口初始化函数
{
	switch(pin)
	{
		case 1: //串口1
		{
			DMA_UR1T_CFG = 0x00; //禁用DMA串口发送中断,最低中断优先级，最低总线优先级
			DMA_UR1T_STA = 0x00;
			DMA_UR1T_AMT = (length-1);
			DMA_UR1T_AMTH = (length-1)>>8;
			DMA_UR1T_TXAH = (u8)((u16)DAT>>8);
			DMA_UR1T_TXAL = (u8)(DAT);

			DMA_UR1T_CR = 0xC0;  //允许DMA，开始发送数据
		}break;
		case 2: //串口2
		{
			DMA_UR2T_CFG = 0x00; //禁用DMA串口发送中断
			DMA_UR2T_STA = 0x00;
			DMA_UR2T_AMT = (length-1);
			DMA_UR2T_AMTH = (length-1)>>8;
			DMA_UR2T_TXAH = (u8)((u16)DAT>>8);
			DMA_UR2T_TXAL = (u8)(DAT);
			DMA_UR2T_CR = 0xC0;
		}break;
		case 3: //串口3
		{
			DMA_UR3T_CFG = 0x00; //禁用DMA串口发送中断
			DMA_UR3T_STA = 0x00;
			DMA_UR3T_AMT = (length-1);
			DMA_UR3T_AMTH = (length-1)>>8;
			DMA_UR3T_TXAH = (u8)((u16)DAT>>8);
			DMA_UR3T_TXAL = (u8)(DAT);
			DMA_UR3T_CR = 0xC0;
		}break;
		case 4: //串口4
		{
			DMA_UR4T_CFG = 0x00; //禁用DMA串口发送中断
			DMA_UR4T_STA = 0x00;
			DMA_UR4T_AMT = (length-1);
			DMA_UR4T_AMTH = (length-1)>>8;
			DMA_UR4T_TXAH = (u8)((u16)DAT>>8);
			DMA_UR4T_TXAL = (u8)(DAT);
			DMA_UR4T_CR = 0xC0;
		}break;
	}
}

void DMA_RXD_init(unsigned char pin,unsigned int length,uchar *DAT)    //DMA接收串口初始化函数
{ 
	switch(pin)
	{
		case 1: //串口1
		{
			DMA_UR1R_CFG = 0x8F;		//bit7 1:使能DMA中断，最低中断优先级，最低总线优先级
			DMA_UR1R_STA = 0x00;
			DMA_UR1R_AMT = (length-1);
			DMA_UR1R_AMTH = (length-1)>>8;
			DMA_UR1R_RXAH = (u8)((u16)DAT>>8);
			DMA_UR1R_RXAL = (u8)(DAT);
			DMA_UR1R_CR = 0xa1;			//bit7 1:使能 UART1_BMM, bit5 1:开始 UART1_BMM 自动接收, bit0 1:清除 FIFO
		}break;
		case 2: //串口2
		{
			DMA_UR2R_CFG = 0x80;		//bit7 1:使能DMA中断
			DMA_UR2R_STA = 0x00;
			DMA_UR2R_AMT = (length-1);
			DMA_UR2R_AMTH = (length-1)>>8;
			DMA_UR2R_RXAH = (u8)((u16)DAT>>8);
			DMA_UR2R_RXAL = (u8)(DAT);
			DMA_UR2R_CR = 0xa1;			//bit7 1:使能 UART1_BMM, bit5 1:开始 UART1_BMM 自动接收, bit0 1:清除 FIFO
		}break;
		case 3: //串口3
		{
			DMA_UR3R_CFG = 0x80;		//bit7 1:使能DMA中断
			DMA_UR3R_STA = 0x00;
			DMA_UR3R_AMT = (length-1);
			DMA_UR3R_AMTH = (length-1)>>8;
			DMA_UR3R_RXAH = (u8)((u16)DAT>>8);
			DMA_UR3R_RXAL = (u8)(DAT);
			DMA_UR3R_CR = 0xa1;			//bit7 1:使能 UART1_BMM, bit5 1:开始 UART1_BMM 自动接收, bit0 1:清除 FIFO
		}break;
		case 4: //串口4
		{
			DMA_UR4R_CFG = 0x80;		//bit7 1:使能DMA中断
			DMA_UR4R_STA = 0x00;
			DMA_UR4R_AMT = (length-1);
			DMA_UR4R_AMTH = (length-1)>>8;
			DMA_UR4R_RXAH = (u8)((u16)DAT>>8);
			DMA_UR4R_RXAL = (u8)(DAT);
			DMA_UR4R_CR = 0xa1;			//bit7 1:使能 UART1_BMM, bit5 1:开始 UART1_BMM 自动接收, bit0 1:清除 FIFO
		}break;
	}
}
