#ifndef		__DELAY_H
#define		__DELAY_H
#include "config.h"
void Delay_X_mS(unsigned int ms);
void Delay_X_uS(unsigned int us);

#endif