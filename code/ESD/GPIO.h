#ifndef		__GPIO_H
#define		__GPIO_H
#include "config.h"
extern void GPIO_init_pin(unsigned char pin,unsigned char mode);   //初始化单个IO引脚函数  
/*
取值：pin:要初始化的引脚   例：11 为 P1^1
取值：mode:要初始化模式    例：0 为 准双向   5mA小电流工作，选择
															 1 为 强推挽   20mA大电流输出，选择
															 2 为 高阻态   禁止电流流入流出，A/D转换选择
															 3 为 开漏     取消内部弱上拉电阻，选择
*/
extern void GPIO_init_8pin(unsigned char pin,unsigned char mode);   //初始化整组IO引脚函数  
/*
取值：pin:要初始化的引脚   例：1 为 P1口
取值：mode:要初始化模式    例：0 为 准双向   5mA小电流工作，选择
															 1 为 强推挽   20mA大电流输出，选择
															 2 为 高阻态   禁止电流流入流出，A/D转换选择
															 3 为 开漏     取消内部弱上拉电阻，选择
*/
extern void GPIO_init_allpin(unsigned char mode);   //初始化所有IO引脚函数  
/*
取值：mode:要初始化模式    例：0 为 准双向   5mA小电流工作，选择
															 1 为 强推挽   20mA大电流输出，选择
															 2 为 高阻态   禁止电流流入流出，A/D转换选择
															 3 为 开漏     取消内部弱上拉电阻，选择
*/
extern void GPIO_isr_init(unsigned char pin,unsigned char mode);  //初始化IO中断
/*
取值：pin:要初始化的引脚   例：11 为 P1^1 因资源问题，仅支持P0、P1、P2、P6、P7口
取值：mode:要初始化模式    例：0 为 下降沿中断
															 1 为 上升沿中断
															 2 为 低电平中断
															 3 为 高电平中断
*/
extern void GPIO_isr_deinit(unsigned char pin);  //反初始化IO中断

extern void GPIO_pull_pin(unsigned char pin,unsigned char mode);   //单个IO上拉配置  
/*
取值：pin:要初始化的引脚   例：11 为 P1^1
取值：mode:要初始化模式    例：0 为 关闭上拉
															 1 为 开启上拉
*/
extern bit Get_IO(unsigned char IO); //读取单个IO状态
extern void Out_IO(unsigned char IO,bit status); //单个IO输出
#endif