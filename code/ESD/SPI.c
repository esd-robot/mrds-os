#include "SPI.h"
void SPI_Init(unsigned char spi_n)//SPI初始化
{	
	SPCTL = 0xd3;
  //IO口切换. 0: P1.2/P5.4 P1.3 P1.4 P1.5, 1: P2.2 P2.3 P2.4 P2.5, 2: P5.4 P4.0 P4.1 P4.3, 3: P3.5 P3.4 P3.3 P3.2
	switch(spi_n)
	{
	case 0:
			P_SW1 |= (0x00<<2);
			break;
	case 1:
			P_SW1 |= (0x01<<2);
			break;
	case 2:
			P_SW1 |= (0x02<<2);
			break;
	case 3:
			P_SW1 |= (0x03<<2);
			break;
	}
	SPSTAT = 0xc0;
}
void SPI_Deinit(void)//SPI反初始化
{
	SPCTL = 0x00;
}
void SPI_WriteByte(unsigned char out)//SPI写1byte数据
{
	SPDAT = out;
	while((SPSTAT & 0x80) == 0);
	SPSTAT = 0xc0;
}
unsigned char ESD_SPI_RW(unsigned char dat)
{
	SPDAT = dat;					//DATA寄存器赋值
	while (!(SPSTAT & 0x80));  		//查询完成标志
	SPSTAT = 0xc0;                  //清中断标志
	return SPDAT;
}
unsigned char SPI_ReadByte(void)//SPI读1byte数据
{
	SPDAT = 0xff;
	while((SPSTAT & 0X80) == 0);
	SPSTAT = 0xc0;
	return SPDAT;
}
void SPI_WriteMultiBytes(unsigned char *tx_ptr, unsigned int n)//SPI连续写数据
{
	unsigned int idata i;
	for(i = 0; i < n; i++)
	{
		SPDAT = tx_ptr[i];
		while((SPSTAT & 0X80) == 0);
		SPSTAT = 0xc0;
	}
}
void SPI_ReadMultiBytes(unsigned char *rx_ptr, unsigned int n)//SPI连续读数据
{
	unsigned int idata i;
	for(i = 0; i < n; i++)
	{
		SPDAT = 0xff;
		while((SPSTAT & 0X80) == 0);
		SPSTAT = 0xc0;
		rx_ptr[i] = SPDAT;
	}
}