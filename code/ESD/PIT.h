#ifndef		__PIT_H
#define		__PIT_H
#include "config.h"

void PIT_init_ms(uint8 tim_n,uint16 time_ms);
void PIT_init_us(uint8 tim_n,uint16 time_us);
uint16 PIT_count_get(uint8 tim_n);
void PIT_init_encoder(uint8 tim_n);
void PIT_count_clean(uint8 tim_n);

extern unsigned int T0_cnt;
extern unsigned int T1_cnt;
extern unsigned int T2_cnt;
extern unsigned int T3_cnt;
extern unsigned int T4_cnt;

#endif