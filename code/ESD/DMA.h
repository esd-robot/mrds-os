#include "config.h"

extern void DMA_TXD_init(unsigned char pin,unsigned int length,uchar *DAT);
extern void DMA_RXD_init(unsigned char pin,unsigned int length,uchar *DAT);  //DMA串口接受初始化函数
/*
取值：pin:要初始化的串口通道   取值范围：0-4  例：0 为串口0
取值：length:自动存储的长度位数，装满后触发DMA中断 取值范围：1-256
取值：DMA_data:数据要存放的位置，类型为数组
*/
