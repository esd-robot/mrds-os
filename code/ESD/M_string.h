#ifndef M_STRING_H
#define M_STRING_H

extern unsigned char _strlen_(char *str);
//返回字符串的长度

extern void _strcat_(char *dest, char *src);
//把src所指字符串添加到dest结尾处(覆盖dest结尾处的'\0')。
//src和dest所指内存区域不可以重叠且，注意dest必须有足够的空间来容纳src的字符串。

extern void _strcpy_(char *dest, char *src);
//将src中的字符拷贝到dest中

extern bit _strcmp_(char *str1, char *str2);
//比较两个字符串是否相等，如果相等，返回0，否则返回1

extern bit _stricmp_(char *str1, char *str2);
//比较两个字符串是否相等，忽略大小写

extern unsigned char _strstr_(char *str1, char *str2);
//从str1中查字符串str2，如果找到，返回起始位置，否则返回0xFF

extern unsigned char _stristr_(char *str1, char *str2);
//从str1中查字符串str2，忽略大小写，如果找到，返回0，否则返回1

extern double _strtod_(char *str);
//为了提高运算速度，保留至多六位小数，多余数位直接丢弃

extern unsigned int _atoi_(char *str);
//将字符串str转换为unsigned int数据,注意，是unsigned int

extern void _itoa_(int value,unsigned char *str);
//将int数据转换成字符串，送到str

extern void _ftos_5_3(float value,char *str);
//将浮点数转换为小数点前保留至多5位，小数点后3位的字符串

extern void _strcpy_plus(unsigned char *dest,unsigned char *phead,unsigned char *ptail);
//给定位置的拷贝，从phead拷贝到ptail，拷贝至dest

extern float _strtof_(char *str);
//为了提高运算速度，保留至多六位小数，多余数位直接丢弃
//第一个字符必须是数字，或'+'或'-',遇到空格或非数字(除第一个小数点外)停止转换

extern bit _strchr_(char *str, char chr);
//从字符串中查找字符，如果找到，返回1，否则返回0;

extern unsigned char *_strichr_(char *str, char chr);
//从字符串中查找字符，忽略大小写，如果找到，返回1，否则返回0

#endif