#ifndef		__IIC_H
#define		__IIC_H
#include "config.h"
typedef enum
{
    IIC_1 = 0,
    IIC_2 = 1,
    IIC_3 = 2,
    IIC_4 = 3,
	
} IICN_enum;

void ESD_IIC_Wait();
void ESD_IIC_Start(); 
void ESD_IIC_Send_data(uchar dat);
void ESD_IIC_Recv_ack();
uchar ESD_IIC_Recv_data();
void ESD_IIC_Send_ack();
void ESD_IIC_Stop();
void ESD_IIC_Send_nack();

void ESD_IIC_WRITE_START_BYTE(uchar dat); //写完整单个数据 = 起始+发送数据+接收ACK组合
void ESD_IIC_WRITE_ONE_BYTE(uchar dat); 	//写单个数据 = 发送数据+接收ACK组合
uchar ESD_IIC_READ_ACK_BYTE();	//接收单个数据 = 接收数据+返回ACK
uchar ESD_IIC_READ_NACK_BYTE();	//接收单个数据 = 接收数据+返回NACK


extern void ESD_Write_IIC(u8 dev, u8 reg, u8 length, u8* dat);//连续写数据
extern void ESD_Read_IIC(u8 dev, u8 reg, u8 length, u8 *dat);//连续读数据
extern void ESD_Init_IIC(IICN_enum iic_n); //初始化IIC


#endif

