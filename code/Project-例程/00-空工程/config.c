#include "config.h"

uint32 sys_clk = MAIN_Fosc; 
uint Key_cnt = 0;				//复位计数值
bit Key_Flag = 0;				//复位标志

char *USER_DEVICEDESC = NULL;
char *USER_PRODUCTDESC = NULL;
char *USER_STCISPCMD = "@STCISP#";                      //设置自动复位到ISP区的用户接口命令

void System_init(void)
{
	//Delay_X_mS(10);
    //set_clk();
		
	WTST = 0;  //设置程序指令延时参数，赋值为0可将CPU执行指令的速度设置为最快
	EAXFR = 1; //扩展寄存器(XFR)访问使能
	CKCON = 0; //提高访问XRAM速度

	P0M1 = 0x00;   P0M0 = 0x00;   //设置为准双向口
	P1M1 = 0x00;   P1M0 = 0x00;   //设置为准双向口
	P2M1 = 0x00;   P2M0 = 0x00;   //设置为准双向口
	P3M1 = 0x00;   P3M0 = 0x00;   //设置为准双向口
	P4M1 = 0x00;   P4M0 = 0x00;   //设置为准双向口
	P5M1 = 0x00;   P5M0 = 0x00;   //设置为准双向口
	P6M1 = 0x00;   P6M0 = 0x00;   //设置为准双向口
	P7M1 = 0x00;   P7M0 = 0x00;   //设置为准双向口

	//USB调试及复位所需代码-----
	EUSB = 1;   //IE2相关的中断使能后，需要重新设置EUSB
	P3M0 &= ~0x03;
	P3M1 |= 0x03;
	IRC48MCR = 0x80;
	while (!(IRC48MCR & 0x01));
	USBCLK=0X00;
	USBCON = 0X90;
	usb_init();
	//-------------------------
	//Delay_X_mS(200);
	PUSB=1;  //usb中断优先级最高
	PUSBH=1;
	EUSB = 1;   //IE2相关的中断使能后，需要重新设置EUSB
	
	
}

void Usb_Rst(void)  //使用USB总线数据复位
{
	if (bUsbOutReady)
	{
			HID_isr(); //分析接收的数据

			usb_OUT_done(); //判断是否为重启数据（固定格式），是则重新启动
	}
}

void Key_Rst(void)		//使用按键扫描复位
{
	
	if(!Reset_PIN)
    {
        if(!Key_Flag)
        {
            Key_cnt++;
            if(Key_cnt >= 1000)		//连续1000ms有效按键检测
            {
                Key_Flag = 1;		//设置按键状态，防止重复触发

                USBCON = 0x00;      //清除USB设置
                USBCLK = 0x00;
                IRC48MCR = 0x00;
                
                Delay_X_mS(10);
                IAP_CONTR = 0x60;   //触发软件复位，从ISP开始执行
                while (1);
            }
        }
    }
    else
    {
        Key_cnt = 0;
        Key_Flag = 0;
    }
}

long Limit_int(long min,long num,long max)  //限幅函数
{
	if(num>max)	num=max;
	if(num<min)	num=min;
	return num;
}

float Limit_float(float min,float num,float max)  //限幅函数
{
	if(num>max)	num=max;
	if(num<min)	num=min;
	return num;
}

uchar BCD2HEX(uchar val)    //BCD转化Byte 
{      
		uchar L;
	L=(val/16)*10+(val%16);
	return L; 
}

uchar HEX2BCD(uchar val)    //B码转化BCD
{      
		uchar L;
	L=(val/10)*16+(val%10);
	return L;
} 

float sq(float num)  //平方函数
{
	return num*num;
}

void set_clk(void)
{
	
	P_SW2 |= 0x80;

	if(sys_clk == 22118400)
	{
		//选择 22.1184MHz
		CLKDIV = 0x04;
		IRTRIM = T22M_ADDR;
		VRTRIM = VRT27M_ADDR;
		IRCBAND = 0x02;
		CLKDIV = 0x00;
	}
	else if(sys_clk == 24000000)
	{
		//选择 24MHz
		CLKDIV = 0x04;
		IRTRIM = T24M_ADDR;
		VRTRIM = VRT27M_ADDR;
		IRCBAND = 0x02;
		CLKDIV = 0x00;
	}
	else if(sys_clk == 27000000)
	{
		//选择 27MHz
		CLKDIV = 0x04;
		IRTRIM = T27M_ADDR;
		VRTRIM = VRT27M_ADDR;
		IRCBAND = 0x02;
		CLKDIV = 0x00;
	}
	else if(sys_clk == 30000000)
	{
	
		//选择 30MHz
		CLKDIV = 0x04;
		IRTRIM = T30M_ADDR;
		VRTRIM = VRT27M_ADDR;
		IRCBAND = 0x02;
		CLKDIV = 0x00;
	}
	else if(sys_clk == 33177600)
	{
		//选择 33.1776MHz
		CLKDIV = 0x04;
		IRTRIM = T33M_ADDR;
		VRTRIM = VRT27M_ADDR;
		IRCBAND = 0x02;
		CLKDIV = 0x00;
	}
	else if(sys_clk == 35000000)
	{
		//选择 35MHz
		CLKDIV = 0x04;
		IRTRIM = T35M_ADDR;
		VRTRIM = VRT44M_ADDR;
		IRCBAND = 0x03;
		CLKDIV = 0x00;
	}
	else
	{
		sys_clk = 35000000;
		//选择 35MHz
		CLKDIV = 0x04;
		IRTRIM = T35M_ADDR;
		VRTRIM = VRT44M_ADDR;
		IRCBAND = 0x03;
		CLKDIV = 0x00;
	}
}