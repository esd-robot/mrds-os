#ifndef		__CONFIG_H
#define		__CONFIG_H

//========================================================================
//                               主时钟定义
//========================================================================
#define SYS_VOL 3.3  	//系统工作电压（V）
//可选24000000L 27000000L 30000000L	33177600L 35000000L
#define MAIN_Fosc		24000000L	//定义主时钟
#define Main_Fosc2	(MAIN_Fosc / 2)  //主时钟/2
#define Main_Fosc4	(MAIN_Fosc / 4)  //主时钟/4
#define Main_Fosc8	(MAIN_Fosc / 8)  //主时钟/8
#define Main_Fosc16	(MAIN_Fosc / 16)  //主时钟/16
#define Main_Fosc_KHZ	(MAIN_Fosc / 1000)  //主时钟KHZ
#define Main_Fosc_MHZ	(MAIN_Fosc / 1000000)  //主时钟MHZ
//                             数据类型宏定义
//========================================================================
#ifndef NULL
 #define NULL ((void *) 0)
#endif
 
typedef bit BOOL;
typedef char int8;
typedef int int16;
typedef long int32;

typedef unsigned char BYTE;
typedef unsigned int WORD;
typedef unsigned long DWORD;

typedef unsigned char u8;
typedef unsigned int u16;
typedef unsigned long u32;

typedef unsigned char uint8;
typedef unsigned int uint16;
typedef unsigned long uint32;

typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned int ushort;
typedef unsigned long ulong;

typedef unsigned char uint8_t;
typedef unsigned int uint16_t;
typedef unsigned long uint32_t; 

#define max(A,B) ((A)>(B)?(A):(B))  //最大值
#define min(A,B) ((A)<(B)?(A):(B))  //最小值

#define PI 3.1415926  //圆周率

//========================================================================
//                                头文件
//========================================================================

#include "STC32G.h"
#include <stdlib.h>
#include <stdio.h>
#include <intrins.h>             //汇编相关头文件
#include <math.h>                //数学相关头文件
#include "string.h"
#include "isr.h"								 //单片机中断相关
#include "GPIO.h"								 //GPI0相关操作函数头文件
#include "PIT.h"
#include "Delay.h"
#include "INT.h"
#include "UART.h"
#include "IIC.h"
#include "PWM.h"
#include "ADC.h"
#include "SPI.h"
#include "usb.h"
#include "M_string.h"
#include "DMA.h"

#include "EEPROM.h"
#include "IIC_OLED.h"
#include "MPU6050.h"
#include "SD.h"
#include "znfat.h"
#include "IIC_HTU21D.h"
#include "IIC_DS3231.h"
#include "IIC_XMC5883.h"
#include "Nrf24L01.h"
#include "IIC_SPL06.h"
#include "IIC_AT24CXX.h"
////========================================================================
////                            引脚和宏定义
////========================================================================

sbit Reset_PIN = P3^2;
sbit KEY1 = P3^2;
sbit KEY2 = P3^6;

sbit LED = P4^5;
sbit BUZZ = P4^4;
sbit PWR_EN = P4^7;
sbit HZ = P3^7;
sbit TF_CD = P4^6;

sbit SW1 = P3^4;
sbit SW2 = P3^5;
sbit SW3 = P2^6;
sbit SW4 = P2^7;

#define OLED_ADDR 0X78
#define QMC5883L_ADDR 0X0C
#define MPU6050_ADDR 0XD2
#define DS3231_ADDR 0XD0
#define SPL06_ADDR 0X76		
	
#define HTU21D_ADDR 0X80
////========================================================================
////                            外部函数和变量声明
////========================================================================
extern void System_init(void);  //系统初始化
extern void Usb_Rst(void);
extern void Key_Rst(void);
extern void set_clk(void);


extern uchar BCD2HEX(uchar val);
extern uchar HEX2BCD(uchar val);
extern long Limit_int(long min,long num,long max); //整数限幅函数
extern float Limit_float(float min,float num,float max);//浮点限幅函数
extern float sq(float num);  //平方函数

extern uint32 sys_clk;





#endif