#ifndef		__ISR_H
#define		__ISR_H
#include "config.h"
/*外部中断*/
extern void INT0_isr();
extern void INT1_isr();
extern void INT2_isr();
extern void INT3_isr();
extern void INT4_isr();
/*定时器中断*/
extern void TM0_isr();
extern void TM1_isr();
extern void TM2_isr();
extern void TM3_isr();
extern void TM4_isr();
/*串口中断*/
extern void UART1_isr();
extern void UART2_isr();
extern void UART3_isr();
extern void UART4_isr();
/*ADC中断*/
extern void ADC_isr();
/*低压检测中断*/
extern void LVD_isr();
/*SPI中断*/
extern void SPI_isr();
/*HID数据解析*/
extern void HID_isr();

extern void DMA_RXD1_isr(void);		//DMA中断响应函数
extern void DMA_RXD2_isr(void);		//DMA中断响应函数
extern void DMA_RXD3_isr(void);		//DMA中断响应函数
extern void DMA_RXD4_isr(void);		//DMA中断响应函数

#endif