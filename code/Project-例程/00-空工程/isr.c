#include "isr.h"

void HID_isr(void)  //USB-HID接收到数据存入UsbOutBuffer[]，进行数据解析处理函数  ，用于设定当前时间 格式：T=2023.06.02.05.11.20.43
{
	uchar nian,yue,ri,zhou,shi,fen,miao;
	
	if(UsbOutBuffer[0]=='T')
	{
		nian = 	(UsbOutBuffer[4]-0x30)*10+(UsbOutBuffer[5]-0x30);
		yue = 	(UsbOutBuffer[7]-0x30)*10+(UsbOutBuffer[8]-0x30);
		ri = 		(UsbOutBuffer[10]-0x30)*10+(UsbOutBuffer[11]-0x30);
		zhou = 	(UsbOutBuffer[13]-0x30)*10+(UsbOutBuffer[14]-0x30);
		shi = 	(UsbOutBuffer[16]-0x30)*10+(UsbOutBuffer[17]-0x30);
		fen = 	(UsbOutBuffer[19]-0x30)*10+(UsbOutBuffer[20]-0x30);
		miao =	(UsbOutBuffer[22]-0x30)*10+(UsbOutBuffer[23]-0x30);
		
		Init_DS3231(nian,yue,ri,zhou,shi,fen,miao); //初始化系统时间
	}
}
void INT0_I() 	interrupt 0  		//外部中断0	
{
  //INT0_isr();
}
void TIMER0_I() interrupt 1   //定时器0中断		
{
	Key_Rst();//按键复位
	Usb_Rst();//通信复位
  TM0_isr();//用户任务
}
void INT1_I()	  interrupt 2  		//外部中断1  	
{
  //INT1_isr();
}
void TIMER1_I() interrupt 3  	 //定时器1中断
{
  //TM1_isr();
}
void UART1_I() interrupt 4   	//串口1中断
{
	if (TI)  
	{
        TI = 0;   
        UART1_OK=0;	        
	}
	if (RI)  
	{
//   UART1_isr();
		 RI = 0;               
	}
}

void ADC_I() 		interrupt 5  		//ADC中断	
{
  //ADC_isr();
}
void LVD_I()    interrupt 6  		//LVD低压检测中断
{
  //LVD_isr();
}
void UART2_I()  interrupt 8  	//串口2中断
{
	if (S2TI)  
	{
			S2TI=0; 
      UART2_OK=0;	
	}
	if (S2RI)   
	{
		//UART2_isr();
			S2RI=0;         
	}
}
void SPI_I() 		interrupt 9  		//SPI中断
{
  //SPI_isr();
}
void INT2_I() 	interrupt 10  		//外部中断2	
{
//  INT2_isr();
}
void INT3_I() 	interrupt 11  	//外部中断3
{
  //INT3_isr();
}
void TIMER2_I() interrupt 12  //定时器2中断	
{
  //TM2_isr();
}
void INT4_I() 	interrupt 16  	//外部中断4
{
  //INT4_isr();
}
void UART3_I() 	interrupt 17  	//串口3中断
{
	if (S3TI)  
	{
		S3TI=0;  
    UART3_OK=0;	
	}
	if (S3RI)   
	{
//		UART3_isr();
			S3RI=0; 
	}
}
void UART4_I() 	interrupt 18  	//串口4中断  	
{
	if (S4TI)  
	{
			S4TI=0;	
      UART4_OK=0;	
	}
	if (S4RI)  
	{
//		UART4_isr();
			S4RI=0;     
	}
}
void TIMER3_I() interrupt 19  //定时器	3中断
{
  //TM3_isr();
}
void TIMER4_I() interrupt 20  //定时器4中断
{
  //TM4_isr();
}
void CMP_I() 		interrupt 21  //比较器中断
{

}
void IIC_I() 		interrupt 24  //IIC总线中断
{

}
/*void USB_I() 		interrupt 25  //USB中断
{

}*/
void PWMA_I() 	interrupt 26  //PWMA中断
{

}
void PWMB_I() 	interrupt 27  //PWMB中断
{

}
void PIN0_I() interrupt 37  	
{	
	unsigned char intf;	
	intf = P0INTF;
	if(intf)
	{
		P0INTF=0;
		if(intf&0x01)
		{
			                   //P00中断
		}
		if(intf&0x02)
		{
			                   //P01中断
		}
		if(intf&0x04)
		{
			                   //P02中断
		}
		if(intf&0x08)
		{
			                   //P03中断
		}
		if(intf&0x10)
		{
			                   //P04中断
		}
		if(intf&0x20)
		{
			                   //P05中断
		}
		if(intf&0x40)
		{
			                   //P06中断
		}
		if(intf&0x80)
		{
			                   //P07中断
		}
	}
}
void PIN1_I() interrupt 38  	
{
	unsigned char intf;	
	intf = P1INTF;
	if(intf)
	{
		P1INTF=0;
		if(intf&0x01)
		{
			                   //P10中断
		}
		if(intf&0x02)
		{
			                   //P11中断
		}
		if(intf&0x04)
		{
			                   //P12中断
		}
		if(intf&0x08)
		{
			                   //P13中断
		}
		if(intf&0x10)
		{
			                   //P14中断
		}
		if(intf&0x20)
		{
			                   //P15中断
		}
		if(intf&0x40)
		{
			                   //P16中断
		}
		if(intf&0x80)
		{
			                   //P17中断
		}
	}	
}
void PIN2_I() interrupt 39  	
{
	unsigned char intf;	
	intf = P2INTF;
	if(intf)
	{
		P2INTF=0;
		if(intf&0x01)
		{
			                   //P20中断
		}
		if(intf&0x02)
		{
			                   //P21中断
		}
		if(intf&0x04)
		{
			                   //P22中断
		}
		if(intf&0x08)
		{
			                   //P23中断
		}
		if(intf&0x10)
		{
			                   //P24中断
		}
		if(intf&0x20)
		{
			                   //P25中断
		}
		if(intf&0x40)
		{
			                   //P26中断
		}
		if(intf&0x80)
		{
			                   //P27中断
		}
	}
}
void PIN3_I() interrupt 40  	
{
	unsigned char intf;
	intf = P3INTF;
	if(intf)
	{
		P3INTF=0;
		if(intf&0x01)
		{
			                   //P30中断
		}
		if(intf&0x02)
		{
			                   //P31中断
		}
		if(intf&0x04)
		{
			                   //P32中断
		}
		if(intf&0x08)
		{
			                   //P33中断
		}
		if(intf&0x10)
		{
			                   //P34中断
		}
		if(intf&0x20)
		{
			                   //P35中断
		}
		if(intf&0x40)
		{
			                   //P36中断
		}
		if(intf&0x80)
		{
			                   //P37中断
		}
	}	
}
void PIN4_I() interrupt 41  	
{
	unsigned char intf;	
	intf = P4INTF;
	if(intf)
	{
		P4INTF=0;
		if(intf&0x01)
		{
			                   //P40中断
		}
		if(intf&0x02)
		{
			                   //P41中断
		}
		if(intf&0x04)
		{
			                   //P42中断
		}
		if(intf&0x08)
		{
			                   //P43中断
		}
		if(intf&0x10)
		{
			                   //P44中断
		}
		if(intf&0x20)
		{
			                   //P45中断
		}
		if(intf&0x40)
		{
			                   //P46中断
		}
		if(intf&0x80)
		{
			                   //P47中断
		}
	}		
}
void PIN5_I() interrupt 42  	
{
	unsigned char intf;	
	intf = P5INTF;
	if(intf)
	{
		P5INTF=0;
		if(intf&0x01)
		{
			                   //P50中断
		}
		if(intf&0x02)
		{
			                   //P51中断
		}
		if(intf&0x04)
		{
			                   //P52中断
		}
		if(intf&0x08)
		{
			                   //P53中断
		}
		if(intf&0x10)
		{
			                   //P54中断
		}
		if(intf&0x20)
		{
			                   //P55中断
		}
	}
}
void PIN6_I() interrupt 43  	
{
	unsigned char intf;
	intf = P6INTF;
	if(intf)
	{
		P6INTF=0;
		if(intf&0x01)
		{
			                   //P60中断
		}
		if(intf&0x02)
		{
			                   //P61中断
		}
		if(intf&0x04)
		{
			                   //P62中断
		}
		if(intf&0x08)
		{
			                   //P63中断
		}
		if(intf&0x10)
		{
			                   //P64中断
		}
		if(intf&0x20)
		{
			                   //P65中断
		}
		if(intf&0x40)
		{
			                   //P66中断
		}
		if(intf&0x80)
		{
			                   //P67中断
		}
	}	
}
void PIN7_I() interrupt 44  	
{
	unsigned char intf;
	intf = P7INTF;
	if(intf)
	{
		P7INTF=0;
		if(intf&0x01)
		{
			                   //P70中断
		}
		if(intf&0x02)
		{
			                   //P71中断
		}
		if(intf&0x04)
		{
			                   //P72中断
		}
		if(intf&0x08)
		{
			                   //P73中断
		}
		if(intf&0x10)
		{
			                   //P74中断
		}
		if(intf&0x20)
		{
			                   //P75中断
		}
		if(intf&0x40)
		{
			                   //P76中断
		}
		if(intf&0x80)
		{
			                   //P77中断
		}
	}	
}



void DMA_M2M_isr(void) interrupt 47
{
	
}

void DMA_ADC_isr(void) interrupt 48
{
	
}

void DMA_SPI_isr(void) interrupt 49
{
	
}

void DMA_UART1_TXD_isr(void) interrupt 50 //DMA-UART1发送完成中断
{
	DMA_UR1T_STA&=~0x01;//清除中断标志
}
void DMA_UART1_RXD_isr(void) interrupt 51
{
	if(DMA_UR1R_STA &0x01) //接收完成
	{
		DMA_UR1R_STA &=~0x01; //清除中断标志
//		DMA_RXD1_isr();    //DMA接收中断响应函数
		DMA_UR1R_CR = 0xa1;
	}
	if(DMA_UR1R_STA &0x02) //接收丢弃
	{
		DMA_UR1R_STA &=~0x02; //清除中断标志
	}
    
}
void DMA_UART2_TXD_isr(void) interrupt 52
{
	DMA_UR2T_STA&=~0x01;//清除中断标志
}
void DMA_UART2_RXD_isr(void) interrupt 53
{
	if(DMA_UR2R_STA &0x01) //接收完成
	{
		DMA_UR2R_STA &=~0x01; //清除中断标志
//		DMA_RXD2_isr();    //DMA接收中断响应函数
		DMA_UR2R_CR = 0xa1;
	}
	if(DMA_UR2R_STA &0x02) //接收丢弃
	{
		DMA_UR2R_STA &=~0x02; //清除中断标志
	}
}
void DMA_UART3_TXD_isr(void) interrupt 54
{
	DMA_UR3T_STA&=~0x01;//清除中断标志
}
void DMA_UART3_RXD_isr(void) interrupt 55
{
	if(DMA_UR3R_STA &0x01) //接收完成
	{
		DMA_UR3R_STA &=~0x01; //清除中断标志
//		DMA_RXD3_isr();    //DMA接收中断响应函数
		DMA_UR3R_CR = 0xa1;
	}
	if(DMA_UR3R_STA &0x02) //接收丢弃
	{
		DMA_UR3R_STA &=~0x02; //清除中断标志
	}
}
void DMA_UART4_TXD_isr(void) interrupt 56
{
	DMA_UR4T_STA&=~0x01;//清除中断标志
}
void DMA_UART4_RXD_isr(void) interrupt 57
{
	if(DMA_UR4R_STA &0x01) //接收完成
	{
		DMA_UR4R_STA &=~0x01; //清除中断标志
//		DMA_RXD4_isr();    //DMA接收中断响应函数
		DMA_UR4R_CR = 0xa1;
	}
	if(DMA_UR4R_STA &0x02) //接收丢弃
	{
		DMA_UR4R_STA &=~0x02; //清除中断标志
	}
}